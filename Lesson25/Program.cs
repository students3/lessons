﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Lesson25
{
    class Program
    {
        //   Asynchronous Programming Model(APM)      => IAsyncResult uses BeginRead, EndRead

        //    Event-based Asynchronous Pattern(EAP)   => myClass.Handler += FileReadCompleted(CompletedEventArgs e)
        //                                               subscribe for event

        //    Task-based Asynchronous Pattern (TAP)   => var numBytes = await fileStream.ReadAsync(buffer, 0, buffer.Length);

        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            sw.Start();

            //TestPLinq();
            //ParallelImageProcessing();
            //OrdinalImageProcessing();
            //TestInvoke("Hello Aleksandra !");
            //BeginEndRead();
            ContinuationTask();

            //var t1 = GetStringAsync();
            //t1.Wait();
            //Console.WriteLine($"MSDN home page answers: {t1.Result}");

            //var t1 = TestAsyncTask();
            //var t2 = new Task(() => { });
            //t2.Start();

            //Task.WaitAll(t1, t2);

            //var numBytes = t1.Result;

            sw.Stop();
            var ts = sw.Elapsed;

            Console.WriteLine($"Finished. It took us: {ts.TotalMilliseconds} miliseconds");
            Console.ReadLine();
        }

        private static void TestPLinq()
        {
            var listTemp = new List<int>();

            for (int i = 0; i < 10000; i++)
            {
                listTemp.Add(i);
            }

            var result = listTemp.AsParallel().Where(x => x % 2 == 0).ToArray();
        }

        private static void ParallelImageProcessing()
        {
            var files = System.IO.Directory.GetFiles(@"C:\Users\Public\Pictures\Sample Pictures", "*.jpg");
            var newDir = @"C:\Users\Public\Pictures\Sample Pictures\Modified";
            System.IO.Directory.CreateDirectory(newDir);

            // Method signature: Parallel.ForEach(IEnumerable<TSource> source, Action<TSource> body)
            // Be sure to add a reference to System.Drawing.dll.
            Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 10 }, currentFile =>
              {
                  // The more computational work you do here, the greater
                  // the speedup compared to a sequential foreach loop.
                  var filename = System.IO.Path.GetFileName(currentFile);
                  var bitmap = new Bitmap(currentFile);

                  bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                  bitmap.Save(System.IO.Path.Combine(newDir, filename));

                  // Peek behind the scenes to see how work is parallelized.
                  // But be aware: Thread contention for the Console slows down parallel loops!!!

                  Console.WriteLine("Processing {0} on thread {1}", filename, Thread.CurrentThread.ManagedThreadId);
                  //close lambda expression and method invocation
              });

            //var dtTemp = new DataTable();

            //Parallel.ForEach(dtTemp.Columns.Cast<DataColumn>(), dr =>
            //{

            //});

            //var arrTemp = new int[] { };

            //Parallel.ForEach(arrTemp.Cast<long>(), itm =>
            //{

            //});

            //Parallel.ForEach(files, new ParallelOptions { MaxDegreeOfParallelism = 10 }, currentfile =>
            //{

            //});
        }

        private static void OrdinalImageProcessing()
        {
            var files = System.IO.Directory.GetFiles(@"C:\Users\Public\Pictures\Sample Pictures", "*.jpg");
            var newDir = @"C:\Users\Public\Pictures\Sample Pictures\Modified";
            System.IO.Directory.CreateDirectory(newDir);

            // Method signature: Parallel.ForEach(IEnumerable<TSource> source, Action<TSource> body)
            // Be sure to add a reference to System.Drawing.dll.
            foreach (var currentFile in files)
            {
                // The more computational work you do here, the greater
                // the speedup compared to a sequential foreach loop.
                var filename = System.IO.Path.GetFileName(currentFile);
                var bitmap = new Bitmap(currentFile);

                bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
                bitmap.Save(System.IO.Path.Combine(newDir, filename));

                // Peek behind the scenes to see how work is parallelized.
                // But be aware: Thread contention for the Console slows down parallel loops!!!

                Console.WriteLine("Processing {0} on thread {1}", filename, Thread.CurrentThread.ManagedThreadId);
                //close lambda expression and method invocation
            }
        }

        private static void TestInvoke(string message)
        {
            // 1) clear temp files on disk
            // 2) clear temp records in DB
            // 3) clear temp managed resources
            Parallel.Invoke(
                () => { Console.WriteLine(message); },
                () => { Console.WriteLine(message); },
                () => DoSomething());
        }

        private static void DoSomething()
        {
            Console.WriteLine("Do something");
        }

        private static void BeginEndRead()
        {
            var buffer = new byte[100];
            var FName = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var FStream = new FileStream(FName, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, FileOptions.Asynchronous);

            IAsyncResult result = FStream.BeginRead(buffer, 0, buffer.Length, null, null);
            int numBytes = FStream.EndRead(result);
            FStream.Close();

            Console.WriteLine("Read {0} Bytes:", numBytes);
            Console.WriteLine(BitConverter.ToString(buffer));
        }

        private static async Task<int> TestAsyncTask()
        {
            // Asynchronous Pattern

            var buffer = new byte[100];
            var fileName = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, FileOptions.Asynchronous);

            var numBytes = await fileStream.ReadAsync(buffer, 0, buffer.Length);

            Console.WriteLine("Read {0} Bytes:", numBytes);
            Console.WriteLine(BitConverter.ToString(buffer));

            return numBytes;
        }

        private static async void TestAsyncVoid()
        {
            // Asynchronous Pattern
            var buffer = new byte[100];
            var fileName = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, 1024, FileOptions.Asynchronous);

            var numBytes = await fileStream.ReadAsync(buffer, 0, buffer.Length);

            Console.WriteLine("Read {0} Bytes:", numBytes);
            Console.WriteLine(BitConverter.ToString(buffer));
        }

        #region Continuations

        private static void ContinuationTask()
        {
            var t1 = new Task<int>(DoOnFirst);
            var t2 = t1.ContinueWith(DoOnSecond);
            var t3 = t2.ContinueWith(DoOnThird);

            var t5 = t1.ContinueWith(DoOnError, TaskContinuationOptions.OnlyOnFaulted);
            t1.Start();

            t3.Wait(); // wait until last task will be finished
        }

        private static int DoOnFirst()
        {
            //Console.WriteLine("Doing first task {0}", Task.CurrentId);
            Console.WriteLine("Doing FIRST task");
            Thread.Sleep(1000);

            if (DateTime.Now.Hour > 12) throw new OperationCanceledException();

            return 5;
        }

        public static int DoOnSecond(Task<int> t)
        {
            if (t.IsFaulted) throw new OperationCanceledException();

            var number = t.Result;
            Console.WriteLine($"Got number: {number} from thread 1");

            //Console.WriteLine("Doing second {0} finished", t.Id);
            Console.WriteLine("Doing SECOND task");
            Thread.Sleep(1000);

            return number + 5;
        }

        public static void DoOnThird(Task<int> t)
        {
            if (t.IsFaulted) return;

            var number = t.Result;
            Console.WriteLine($"Got number: {number} from thread 2");

            Console.WriteLine("Doing THIRD task");
            //Console.WriteLine("Doing THIRD {0} finished", t.Id);
            Thread.Sleep(1000);
        }

        public static void DoOnError(Task t)
        {
            Console.WriteLine("task {0} had an error!", t.Id);
            Console.WriteLine("my id {0}", Task.CurrentId);
            Console.WriteLine("do some cleanup");
        }

        #endregion Continuations

        private static async Task<string> GetStringAsync()
        {
            var client = new HttpClient();
            var t1 = client.GetStringAsync("http://msdn.microsoft.com");

            DoInpependentWork();

            return await t1;
        }

        private static void DoInpependentWork()
        {
            Console.WriteLine("Some work ...");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace Lesson25
{
    // https://blogs.msdn.microsoft.com/dotnet/2012/06/06/async-in-4-5-enabling-progress-and-cancellation-in-async-apis/
    class ReportingProgress
    {
        private static async Task<int> UploadPicturesAsync(List<Image> imageList, IProgress<int> progress)
        {
            int totalCount = imageList.Count;
            int processCount = await Task.Run<int>(() =>
            {
                int tempCount = 0;
                foreach (var image in imageList)
                {
                    //await the processing and uploading logic here
                    //int processed = await UploadAndProcessAsync(image);
                    progress?.Report((tempCount * 100 / totalCount));

                    tempCount++;
                }

                return tempCount;
            });
            return processCount;
        }
    }

    //private static void async UploadAndProcessAsync(Image image)
    //{
    //    // do smth
    //}
}

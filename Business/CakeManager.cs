﻿using System;
using Business.Interfaces;

namespace Business
{
    public class CakeManager : IManager
    {
        public void SellProduct(IProduct product, int qty)
        {
            product.DateSold = DateTime.Now;
            product.Qty = qty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;

namespace TestXAML
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly List<string> _listImages = new List<string>();

        public MainWindow()
        {
            InitializeComponent();

            _listImages = this.SearchImages();
        }


        public void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            // prev image
        }

        public void btnNext_Click(object sender, RoutedEventArgs e)
        {
            // next image
        }



        private void btnMdi_Click(object sender, RoutedEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnList_Click(object sender, RoutedEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnNav_Click(object sender, RoutedEventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DisplayImage(string fileName)
        {
            var bmpImage = new BitmapImage(new Uri(fileName));
            Image1.Source = bmpImage;
            ImagePath.Content = fileName;
        }

        private List<string> SearchImages()
        {
            var result =new List<string>();
            var root = Environment.GetFolderPath(Environment.SpecialFolder.CommonPictures);
            var path = System.IO.Path.Combine(root, "Sample Pictures");

            if (System.IO.Directory.Exists(path))
            {
                result = System.IO.Directory.GetFiles(path, "*.jpg").ToList();
            }

            return result;
        }

        private void Window_Activated(object sender, EventArgs e)
        {

        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = true;
        }
    }
}

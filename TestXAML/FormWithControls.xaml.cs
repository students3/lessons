﻿using System.Data;
using System.Linq;
using System.Windows;

namespace TestXAML
{
    /// <summary>
    /// Interaction logic for FormWithControls.xaml
    /// </summary>
    public partial class FormWithControls : Window
    {
        public FormWithControls()
        {
            InitializeComponent();
        }

        private DataTable _dtSource;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            _dtSource = this.GenerateDatable();
            _dtSource.DefaultView.RowFilter = "UserName = 'Sergey'";

            Grid1.DataContext = _dtSource.DefaultView;
        }

        private DataTable GenerateDatable()
        {
            var dt = new DataTable();
            var dc = dt.Columns.Add("TaskId", typeof(int));
            dc.AutoIncrement = true;
            dc.AutoIncrementSeed = 1;

            dt.Columns.Add("TaskName", typeof(string));
            dt.Columns.Add("UserName", typeof(string));

            var dr = dt.NewRow();
            //dr["TaskId"] = 1;
            dr["TaskName"] = "Water flowers";
            dr["UserName"] = "Sergey";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            //dr["TaskId"] = 2;
            dr["TaskName"] = "Go to the gym";
            dr["UserName"] = "Kostya";
            dt.Rows.Add(dr);


            dr = dt.NewRow();
            //dr["TaskId"] = 2;
            dr["TaskName"] = "Go to the gym";
            dr["UserName"] = "Sergey";
            dt.Rows.Add(dr);


            dt.AcceptChanges();

            return dt;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var rowsModified = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Modified);
            var rowsAdded = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Added);
            var rowsDeleted = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Deleted);
            //var rowsDetached = _dtSource.AsEnumerable().Count(x => x.RowState == DataRowState.Detached);

            if (rowsModified == 0)
            {
                MessageBox.Show($"You didn't modify any row", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                _dtSource.AcceptChanges();
            }
            else
            {
                MessageBox.Show($"You modifed {rowsModified} rows", "Save changes ?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
        }
    }
}

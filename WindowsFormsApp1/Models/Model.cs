﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1.Models
{
    internal class Model     // UserManager
    {
        public List<string> UserNames
        {
            get { return GetUsers().Keys.ToList(); }
        }

        public Model()
        {

        }   // IUserRepository

        public Dictionary<string, string> GetUsers()
        {
            return new Dictionary<string, string>
            {
                {"User1", "Password1"}
            };
        }

        public string TryToLogin(string user, string password)
        {
            var users = GetUsers();
            return users.ContainsKey(user) && users[user] == password ? "Login Successful!" : "Login Failed";
        }
    }
}

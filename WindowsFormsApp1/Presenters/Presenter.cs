﻿using System;
using WindowsFormsApp1.Interfaces;
using WindowsFormsApp1.Models;

namespace WindowsFormsApp1.Presenters
{
    internal class Presenter             // Controller
    {
        private readonly IView _view;
        private readonly Model _model;   // UserManager

        public Presenter(IView view)
        {
            _view = view;
            _model = new Model();

            // subscribe
            view.OnUpdateUserList += OnUpdateUsers;
            view.OnTryLogin += OnLogin;
        }

        private void OnLogin(object sender, EventArgs e)
        {
            string loginResult = _model.TryToLogin(_view.GetUser(), _view.GetPassword());
            _view.ShowMessage(loginResult);
        }

        private void OnUpdateUsers(object sender, EventArgs e)
        {
            _view.UpdateUsers(_model.UserNames);
        }
    }
}

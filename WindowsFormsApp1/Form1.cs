﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WindowsFormsApp1.Interfaces;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form, IView
    {
        public Form1()
        {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> OnUpdateUserList = delegate { };
        public event EventHandler<EventArgs> OnTryLogin = delegate { };

        #region Form events

        private void Form1_Load(object sender, System.EventArgs e)
        {
            OnUpdateUserList?.Invoke(sender, e);
        }

        #endregion Form events

        #region Controls

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            //if (e != null)
            //{
            //    MessageBox.Show("Button clicked with " + e.Button, "Title");
            //}
            //else
            //{
            //    MessageBox.Show("Button clicked", "Title");
            //}
        }

        #endregion Controls

        #region Methods

        private void DoSomeWork()
        {
            #region Processing

            #region Processing2

            #endregion Processing2

            #endregion Processing
        }

        #endregion Methods

        string IView.GetUser() { return textBox1.Text; }
        string IView.GetPassword() { return maskedTextBox1.Text; }

        void IView.UpdateUsers(List<string> users)
        {
            listBox1.Items.Clear();

            foreach (var user in users)
            {
                listBox1.Items.Add(user);
            }
        }
        void IView.ShowMessage(string message)
        {
            MessageBox.Show(message, "Login Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OnTryLogin?.Invoke(sender, e);
        }


        //protected virtual void HandleUpdateUserList(object sender, EventArgs e)
        //{
        //    OnUpdateUserList?.Invoke(sender, e);
        //}
    }
}

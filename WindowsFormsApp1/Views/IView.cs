﻿using System;
using System.Collections.Generic;

namespace WindowsFormsApp1.Interfaces
{
    public interface IView
    {
        // set of properties
        string GetUser();
        string GetPassword();

        // set of methods
        void UpdateUsers(List<string> users);
        void ShowMessage(string message);

        // set of events
        event EventHandler<EventArgs> OnUpdateUserList;
        event EventHandler<EventArgs> OnTryLogin;
    }
}

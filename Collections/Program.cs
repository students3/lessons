﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestList();

            //List<Product> listProducts = GenerateProducts();
            //Console.WriteLine();
            //string searchName = listProducts[1450000].Name;

            //TestHashSet(listProducts, searchName);
            //Console.WriteLine();

            //TestList(listProducts, searchName);
            //Console.WriteLine();

            //TestDictionary(listProducts, searchName);
            //Console.WriteLine();

            TestArrayResize();
            Console.ReadLine();
        }

        private static void TestList()
        {
            //List<string> result = new List<string>();
            List<string> result = new List<string>() { "Vasya", "Vasya", "Petya", "Kolya", "", "" };

            result.AddRange(new string[] { "Peter", "John" });
            result.Remove("Peter");
            result.Contains("Peter");

            var j = result.IndexOf("Petya");
            result = result.Except(new[] { string.Empty }).ToList();
        }

        private static List<Product> GenerateProducts()
        {
            var sw = new Stopwatch();
            sw.Start();
            var result = new List<Product>();

            for (int i = 0; i < 2000000; i++)
            {
                result.Add(new Product
                {
                    Id = i + 1,
                    Name = "'" + Guid.NewGuid().ToString().Replace("-", " ") + "'"
                });
            }

            sw.Stop();
            double seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);
            Console.WriteLine($"Generating Products took us: {seconds} seconds");

            return result;
        }

        private static bool TestHashSet(List<Product> listProducts, string searchName)
        {
            bool result = false;

            var sw = new Stopwatch();
            sw.Start();
            HashSet<string> hsNames = new HashSet<string>(listProducts.Select(x => x.Name));
            sw.Stop();
            double seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);
            Console.WriteLine($"Creating Hashset took us: {seconds} seconds");


            sw = new Stopwatch();
            sw.Start();
            result = hsNames.Contains(searchName);
            sw.Stop();
            seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);

            var message = result ? $"Element {searchName} found" : $"Not found {searchName}";

            Console.WriteLine($"Searching in Hashset took us: {seconds} seconds");
            Console.WriteLine(message);

            return result;
        }

        private static Product TestList(List<Product> listProducts, string searchName)
        {
            Product result = null;

            var sw = new Stopwatch();
            sw.Start();

            foreach (var item in listProducts)
            {
                if (string.Equals(item.Name, searchName, StringComparison.InvariantCultureIgnoreCase))
                {
                    result = item;
                    break;
                }
            }

            sw.Stop();
            double seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);

            var message = result != null ? $"Element {searchName} found" : $"Not found {searchName}";

            Console.WriteLine($"Searching in List took us: {seconds} seconds");
            Console.WriteLine(message);

            return result;
        }

        private static Product TestDictionary(List<Product> listProducts, string searchName)
        {
            Product result = null;

            var sw = new Stopwatch();
            sw.Start();
            Dictionary<string, Product> dicProducts = listProducts.ToDictionary(x => x.Name, x => x);
            sw.Stop();
            double seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);
            Console.WriteLine($"Creating Dictionary took us: {seconds} seconds");

            sw = new Stopwatch();
            sw.Start();
            if (dicProducts.ContainsKey(searchName))
            {
                result = dicProducts[searchName];
            }

            sw.Stop();
            seconds = Math.Round(sw.Elapsed.TotalSeconds, 2);

            var message = result != null ? $"Element {searchName} found" : $"Not found {searchName}";

            Console.WriteLine($"Searching in Dictionary took us: {seconds} seconds");
            Console.WriteLine(message);

            return result;
        }

        private static void TestArrayResize()
        {
            int[] numbers2 = new int[8];
            int[] numbers = { 1, 2, 5 };

            var arrayHelper = new IntegerArrayHelper();
            numbers = arrayHelper.AddItem(numbers, 8);

            foreach (var item in numbers)
            {
                Console.WriteLine(item);
            }

            string[] fruits = { "banana", "grape", "cherry" };
            var arrayHelper2 = new StringArrayHelper();
            fruits = arrayHelper2.AddItem(fruits, "apple");

            foreach (var item in fruits)
            {
                Console.WriteLine(item);
            }

            List<string> vegetables = new List<string>(new string[] { null, null, null, null, null, null });
            //vegetables.TrimExcess();
            vegetables.Insert(5, "pumpkin");
            vegetables.Insert(5, "potato");

            var elem = vegetables.FirstOrDefault(x => x == "pump");
        }
    }
}

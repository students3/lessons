﻿namespace Collections
{
    public abstract class AbstractArrayHelper<T>
    {
        public T[] AddItem(T[] arrInput, T value)
        {
            T[] arrInputCopy = new T[arrInput.Length + 1];

            // fill numbersCopy
            for (var i = 0; i < arrInput.Length; i++)
            {
                arrInputCopy[i] = arrInput[i];
            }

            // add new element
            arrInputCopy[arrInputCopy.Length - 1] = value;

            arrInput = arrInputCopy;

            return arrInput;
        }
    }
}

﻿using Lesson9.Handlers;

namespace Lesson9.Models
{
    public class Pig
    {
        public delegate void PigWeightChangedHandler(object sender, PigWeightChangedEventArgs e);
        public event PigWeightChangedHandler WeightChanged;

        public PigOwner Owner { get; private set; }

        public Pig(PigOwner owner)
        {
            this.Owner = owner;
        }

        private int weight;
        private bool NeedsSaving;

        public int Weight
        {
            get
            {
                return weight;
            }
            set
            {
                if (weight != value)
                {
                    var args = new PigWeightChangedEventArgs
                    {
                        OldWeight = weight,
                        NewWeight = value
                    };

                    if (this.WeightChanged != null) this.WeightChanged(this, args);
                    NeedsSaving = true;
                }

                weight = value;
            }
        }
    }
}

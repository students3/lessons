﻿using Lesson9.Handlers;

namespace Lesson9.Models
{
    public class MeatCollector
    {
        public string Name { get; set; }

        /// <summary>
        /// Method of subscribing to pigs weight changed
        /// </summary>
        /// <param name="sender">Pig</param>
        /// <param name="e"></param>
        public void Notify(object sender, PigWeightChangedEventArgs e)
        {
            if (sender != null && sender is Pig)
            {
                var pig = sender as Pig;

                if (e.NewWeight >= 100)
                {
                    System.Console.WriteLine($"Hello {pig.Owner.Name} my name is: {Name} I'm ready to collect your pig");
                }
            }
        }
    }
}

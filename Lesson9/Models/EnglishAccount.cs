﻿namespace Lesson9.Models
{
    public class EnglishAccount : Account
    {
        public EnglishAccount(int sum) : base(sum)
        {
        }

        protected override void OnAdded(int sum, string message)
        {
            string englMessage = $"Added to account {sum} dollars";
            base.OnAdded(sum, englMessage);
        }

        protected override void OnWithdrawn(int sum, string message)
        {
            string englMessage = sum > 0 ? $"Withdrawn {sum} dollars" : $"Insufficient amount on account";
            base.OnWithdrawn(sum, $"Withdrawn {sum} dollars");
        }
    }
}

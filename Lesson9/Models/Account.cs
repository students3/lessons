﻿namespace Lesson9.Models
{
    public class Account
    {
        // Объявляем делегат
        public delegate void AccountStateHandler(int sum, string message);

        // Событие, возникающее при выводе денег
        public event AccountStateHandler OnFundsWithdrawn;

        // Событие, возникающее при добавление на счет
        public event AccountStateHandler OnFundsAdded;

        int _sum; // Переменная для хранения суммы

        public Account(int sum)
        {
            _sum = sum;
        }

        public int CurrentSum
        {
            get { return _sum; }
        }

        public void Put(int sum)
        {
            _sum += sum;
            this.OnAdded(sum, $"На счет поступило {sum}");
        }

        public void Withdraw(int sum)
        {
            if (sum <= _sum)
            {
                _sum -= sum;
                this.OnWithdrawn(sum, $"Сумма {sum} снята со счета");
            }
            else
            {
                this.OnWithdrawn(-1, "Недостаточно денег на счете");
            }
        }

        protected virtual void OnWithdrawn(int sum, string message)
        {
            AccountStateHandler handler = OnFundsWithdrawn;
            if (handler != null) handler(sum, message);
        }

        protected virtual void OnAdded(int sum, string message)
        {
            AccountStateHandler handler = OnFundsAdded;
            if (handler != null) handler(sum, message);
        }
    }
}

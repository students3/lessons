﻿using Lesson9.Models;
using System;

namespace Lesson9
{
    class Program
    {
        static void Main(string[] args)
        {
            AccountEvents();
            //MeatCollector();
            Console.ReadLine();
        }

        static void MeatCollector()
        {
            var pigOwner = new PigOwner() { Name = "Vasiliy Petrovich" };
            var pig = new Pig(pigOwner);

            var meatCollector = new MeatCollector { Name = "Big Boss" };
            pig.WeightChanged += new Pig.PigWeightChangedHandler(meatCollector.Notify);

            Console.WriteLine("Pig weight: 10");
            pig.Weight = 10;

            Console.WriteLine("Pig weight: 50");
            pig.Weight = 50;

            Console.WriteLine("Pig weight: 120");
            pig.Weight = 120;

            pig.WeightChanged -= new Pig.PigWeightChangedHandler(meatCollector.Notify);

            Console.WriteLine("Pig weight: 150");
            pig.Weight = 150;
        }

        static void AccountEvents()
        {
            var account = new EnglishAccount(200);

            // Добавляем обработчики события
            account.OnFundsAdded += new Account.AccountStateHandler(Show_Message); // полная форма
            account.OnFundsAdded += Log_Message;
            //account.Added += Log_Message;
            //account.Added += Log_Message;
            account.OnFundsWithdrawn += Show_Message;                              // сокращенна форма

            account.Put(50);

            account.Withdraw(100);

            // Удаляем обработчик события
            account.OnFundsWithdrawn -= Show_Message;

            account.Withdraw(500);
            account.Put(150);

            Console.ReadLine();
        }

        private static void Show_Message(int sum, string message)
        {
            Console.WriteLine(message);
        }

        private static void Log_Message(int sum, string message)
        {
            Console.WriteLine("Logging message: " + message);
        }
    }
}

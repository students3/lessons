﻿namespace Lesson9.Handlers
{
    public class PigWeightChangedEventArgs
    {
        public int OldWeight { get; set; }
        public int NewWeight { get; set; }
    }
}

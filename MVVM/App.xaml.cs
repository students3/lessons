﻿using System.Windows;
using MVVM.View;
using MVVM.ViewModel;

namespace MVVM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var mwvm = new MainWindowView
            {
                DataContext = new MainWindowPresenter()
            };
            mwvm.Show();
        }
    }
}

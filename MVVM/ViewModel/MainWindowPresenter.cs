﻿using System.Windows;
using System.Windows.Input;
using MVVM.Model;

namespace MVVM.ViewModel
{
    class MainWindowPresenter
    {
        public MainWindowPresenter()
        {
            ClickCommand = new Command(x => ClickMethod());

            Person = new PersonModel
            {
                FirstName = "Viktor",
                LastName = "Ivanov"
            };
        }

        public PersonModel Person { get; set; }

        public ICommand ClickCommand { get; set; }

        private void ClickMethod()
        {
            MessageBox.Show("This is click command.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Reg = System.Text.RegularExpressions;

namespace Lesson14
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestRegExReplace();
            TestNamedGroups();
            Console.ReadLine();
        }

        private static void TestRegExMatch()
        {
            //    ^[\w| ]{1,}$   =>   hello word again
            //    ^[\w|\s|\d|-]{1,}$
            var regex = new Reg.Regex(@"\d+", Reg.RegexOptions.IgnoreCase);
            var input = "Мама купила в 2 магащинах 10 кг яблок";

            if (regex.IsMatch(input))
            {
                Reg.MatchCollection matches = regex.Matches(input);

                foreach (Reg.Match itm in matches)
                {
                    Console.WriteLine($"Position: {itm.Index}, Value: {itm.Value}");
                }
            }
            else
            {
                Console.WriteLine("Matches not found");
            }
        }

        private static void TestRegExReplace()
        {
            //    ^[\w| ]{1,}$   =>   hello word again
            //    ^[\w|\s|\d|-]{1,}$
            var regex = new Reg.Regex(@"\d{1,}", Reg.RegexOptions.IgnoreCase);
            var input = "Мама купила в 2 магащинах 10 кг яблок";

            var result = regex.Replace(input, "{number}");

            Console.WriteLine(result);
        }

        private static void TestNamedGroups()
        {
            //    ^[\w| ]{1,}$   =>   hello word again
            //    ^[\w|\s|\d|-]{1,}$
            var regex = new Reg.Regex(@"(?<who>Мама|Папа)?.*(?<what>\s\d{1,}.*)", Reg.RegexOptions.IgnoreCase | Reg.RegexOptions.Multiline);
            var input = "Мама купила в 10 кг яблок \r\n" +
                        "Бабушка купила 2 собаки";

            if (regex.IsMatch(input))
            {
                var groupNames = regex.GetGroupNames();
                Reg.MatchCollection matches = regex.Matches(input);

                foreach (Reg.Match itm in matches)
                {
                    Console.WriteLine($"Who: {itm.Groups["who"].Value ?? ""}, What: {itm.Groups["what"].Value}");
                }
            }
            else
            {
                Console.WriteLine("Matches not found");
            }
        }

        private static void TestFolderName(string input)
        {
            var invalidChars = System.IO.Path.GetInvalidFileNameChars();
            var arrChars = input.ToCharArray();

            if (arrChars.Any(x => invalidChars.Contains(x)))
            {
                var foundChars = new List<char>();

                foreach (var ch in arrChars.Distinct().ToArray())
                {
                    if (invalidChars.Contains(ch))
                    {
                        foundChars.Add(ch);
                    }
                }

                Console.WriteLine($"File name: {input} is invalid !");
            }
            else
            {
                Console.WriteLine($"File name: {input} is valid !");
            }
        }
    }
}

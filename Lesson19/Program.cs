﻿using EF.CodeFirst.Context;
using EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson19
{
    class Program
    {
        static void Main(string[] args)
        {
            TestUpdate();
            //TestContext2();

            //GetEmails();
            //GetLecturers();
            //TestContext();
            //TestStrings();

            Console.WriteLine("Finished");
            Console.ReadLine();

            //PM> Enable-Migrations
            //PM> Add-Migration ElseTables
            //PM> Update-Database –Verbose

        }

        private static void TestContext()
        {
            using (var context = new UnivercityContext())
            {
                var email = new Email
                {
                    EmailValue = "sdf@gmail.com"
                };
                var lecturer = new Lecturer
                {
                    FirstName = "Ivan",
                    LastName = "Petrov",
                    City = "Nikolayev",
                    Phone = "988-98-988",
                    State = "NI",
                    Zip = "54029",
                    Address = "Frunze Str. 12, apt. 56",
                    Emails = new List<Email>(new Email[] { email })
                };

                context.Lecturers.Add(lecturer);
                context.SaveChanges();
            }
        }

        private static void TestContext2()
        {
            using (var context = new UnivercityContext())
            {
                var email = new Email
                {
                    EmailValue = "alla@gmail.com"
                };
                var lecturer = new Lecturer
                {
                    FirstName = "Alla",
                    LastName = "Petrova",
                    City = "Nikolayev",
                    Phone = "988-98-988",
                    State = "NI",
                    Zip = "54029",
                    Address = "Frunze Str. 12, apt. 56",
                    Emails = new List<Email>(new Email[] { email }),
                    Age = 25
                };

                context.Lecturers.Add(lecturer);
                context.SaveChanges();
            }
        }

        private static void TestStrings()
        {
            var apple = "Mama bought, some milk.";
            var part = apple.Substring(0, apple.IndexOf(" "));

            var arrTemp = apple.Split(new char[] { ' ', ',', '.' }, StringSplitOptions.RemoveEmptyEntries);
            part = arrTemp.FirstOrDefault() ?? string.Empty;

            var mmy = "Chevy Camaro / \\;;;";
            var result = mmy.TrimEnd(new char[] { '/', '\\', ' ', ';' });
            Console.WriteLine(result);
        }

        private static void GetEmails()
        {
            using (var context = new UnivercityContext())
            {
                var emails = context.Emails.ToList<Email>();

                foreach (Email item in emails)
                {
                    Console.WriteLine(item.Lecturer.FirstName + " " + item.Lecturer.LastName);
                }
            }
        }

        private static void GetLecturers()
        {
            using (var context = new UnivercityContext())
            {
                var lecturers = context.Lecturers.Where(x => x.City == "Kiev").ToList<Lecturer>();

                foreach (var item in lecturers)
                {
                    Console.WriteLine(item.FirstName + " " + item.LastName);
                }
            }
        }

        private static void TestUpdate()
        {
            using (var context = new UnivercityContext())
            {
                var lecturers = context.Lecturers.ToList<Lecturer>();

                foreach (var item in lecturers)
                {
                    item.Age = 25;
                }

                context.SaveChanges();
            }
        }

        private static void TestTransaction()
        {
            using (var context = new UnivercityContext())
            {
                using (var trans = context.Database.BeginTransaction())
                {
                    try
                    {
                        var email = new Email
                        {
                            EmailValue = "alexey@gmail.com"
                        };
                        var lecturer = new Lecturer
                        {
                            FirstName = "Alexey",
                            LastName = "Petrov",
                            City = "Nikolayev",
                            Phone = "988-98-988",
                            State = "NI",
                            Zip = "54029",
                            Address = "Frunze Str. 12, apt. 56",
                            Emails = new List<Email>(new Email[] { email }),
                            Age = 30
                        };

                        context.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                    }
                }
            }
        }
    }
}

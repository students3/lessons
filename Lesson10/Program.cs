﻿using Lesson10.Delegates;
using System;
using System.Linq;

namespace Lesson10
{
    class Program
    {
        delegate void factor(int j);


        static void Main(string[] args)
        {
            InlineFunction();
            //TestUnaryOperators();
            Console.ReadLine();
        }

        private static void TestUnaryOperators()
        {
            var myClass = new UnaryOperations();
            var myDelegate = new UnOpHandler(myClass.Increment);

            int j = myDelegate(2);
            int k = myClass.Increment(2);

            Console.WriteLine("From delegate: " + j);
            Console.WriteLine("From class: " + k);

            var arrTemp = new int[] { 565, 54, 54, 888 };
            var result = arrTemp.Select(vasya => myClass.Increment(vasya)).ToArray();
        }

        private static void InlineFunction()
        {
            factor fact3 = delegate (int j)
            {
                for (int i = 1; i <= 3; ++i)
                {
                    j = j * i;
                    Console.WriteLine(j);
                }
            };

            //Pay attention to ;
            fact3(1);

            //Some code
        }

        delegate bool Lambda_ex(int v);

        private static void LamdaDelegates()
        {
            Lambda_ex ndiv2 = x => x % 3 == 0; // Lamdba expression
            Console.WriteLine("Lambda expression ");

            for (int i = 1; i <= 10; i++)
            {
                if (ndiv2(i)) Console.WriteLine(i);
            }
        }
    }
}

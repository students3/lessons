﻿namespace Lesson10.Delegates
{
    public delegate int UnOpHandler(int i);

    class UnaryOperations
    {
        public int Increment(int i)
        {
            return ++i;
        }

        public int Decrement(int i)
        {
            return i--;
        }
    }
}

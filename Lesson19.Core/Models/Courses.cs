﻿using System;
using System.Collections.Generic;

namespace Lesson19.Core.Models
{
    public partial class Courses
    {
        public Courses()
        {
            CourseLecturers = new HashSet<CourseLecturers>();
        }

        public string CourseId { get; set; }
        public string CourseName { get; set; }
        public string Type { get; set; }
        public string FaclId { get; set; }
        public int? Size { get; set; }
        public decimal? Marks { get; set; }
        public int? Quantity { get; set; }
        public DateTime? BeginDate { get; set; }
        public short Contract { get; set; }

        public Faculties Facl { get; set; }
        public ICollection<CourseLecturers> CourseLecturers { get; set; }
    }
}

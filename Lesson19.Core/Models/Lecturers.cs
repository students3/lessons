﻿using System;
using System.Collections.Generic;

namespace Lesson19.Core.Models
{
    public partial class Lecturers
    {
        public Lecturers()
        {
            CourseLecturers = new HashSet<CourseLecturers>();
            Email = new HashSet<Email>();
        }

        public string LcId { get; set; }
        public string LcFname { get; set; }
        public string LcLname { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public ICollection<CourseLecturers> CourseLecturers { get; set; }
        public ICollection<Email> Email { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Lesson19.Core.Models
{
    public partial class Lab5Context : DbContext
    {
        public virtual DbSet<CourseLecturers> CourseLecturers { get; set; }
        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<Email> Email { get; set; }
        public virtual DbSet<Faculties> Faculties { get; set; }
        public virtual DbSet<Lecturers> Lecturers { get; set; }
        public virtual DbSet<MigrationHistory> MigrationHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=(local);Initial Catalog=Lab5;Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CourseLecturers>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.LcId });

                entity.ToTable("course_lecturers");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IX_course_id");

                entity.HasIndex(e => e.LcId)
                    .HasName("IX_lc_id");

                entity.Property(e => e.CourseId)
                    .HasColumnName("course_id")
                    .HasMaxLength(128);

                entity.Property(e => e.LcId)
                    .HasColumnName("lc_id")
                    .HasMaxLength(128);

                entity.Property(e => e.LcOrder).HasColumnName("lc_order");

                entity.Property(e => e.Share).HasColumnName("share");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.CourseLecturers)
                    .HasForeignKey(d => d.CourseId)
                    .HasConstraintName("FK_dbo.course_lecturers_dbo.courses_course_id");

                entity.HasOne(d => d.Lc)
                    .WithMany(p => p.CourseLecturers)
                    .HasForeignKey(d => d.LcId)
                    .HasConstraintName("FK_dbo.course_lecturers_dbo.lecturers_lc_id");
            });

            modelBuilder.Entity<Courses>(entity =>
            {
                entity.HasKey(e => e.CourseId);

                entity.ToTable("courses");

                entity.HasIndex(e => e.FaclId)
                    .HasName("IX_facl_id");

                entity.Property(e => e.CourseId)
                    .HasColumnName("course_id")
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.BeginDate)
                    .HasColumnName("begin_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Contract).HasColumnName("contract");

                entity.Property(e => e.CourseName).HasColumnName("course_name");

                entity.Property(e => e.FaclId)
                    .HasColumnName("facl_id")
                    .HasMaxLength(128);

                entity.Property(e => e.Marks).HasColumnName("marks");

                entity.Property(e => e.Quantity).HasColumnName("quantity");

                entity.Property(e => e.Size).HasColumnName("size");

                entity.Property(e => e.Type).HasColumnName("type");

                entity.HasOne(d => d.Facl)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.FaclId)
                    .HasConstraintName("FK_dbo.courses_dbo.faculties_facl_id");
            });

            modelBuilder.Entity<Email>(entity =>
            {
                entity.HasKey(e => e.EmId);

                entity.ToTable("email");

                entity.HasIndex(e => e.LcId)
                    .HasName("IX_lc_id");

                entity.Property(e => e.EmId).HasColumnName("em_Id");

                entity.Property(e => e.EmValue).HasColumnName("em_value");

                entity.Property(e => e.LcId)
                    .HasColumnName("lc_id")
                    .HasMaxLength(128);

                entity.HasOne(d => d.Lc)
                    .WithMany(p => p.Email)
                    .HasForeignKey(d => d.LcId)
                    .HasConstraintName("FK_dbo.email_dbo.lecturers_lc_id");
            });

            modelBuilder.Entity<Faculties>(entity =>
            {
                entity.HasKey(e => e.FaclId);

                entity.ToTable("faculties");

                entity.Property(e => e.FaclId)
                    .HasColumnName("facl_id")
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.Country).HasColumnName("country");

                entity.Property(e => e.FaclName).HasColumnName("facl_name");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.University).HasColumnName("university");
            });

            modelBuilder.Entity<Lecturers>(entity =>
            {
                entity.HasKey(e => e.LcId);

                entity.ToTable("lecturers");

                entity.Property(e => e.LcId)
                    .HasColumnName("lc_id")
                    .HasMaxLength(128)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.City).HasColumnName("city");

                entity.Property(e => e.LcFname).HasColumnName("lc_fname");

                entity.Property(e => e.LcLname).HasColumnName("lc_lname");

                entity.Property(e => e.Phone).HasColumnName("phone");

                entity.Property(e => e.State).HasColumnName("state");

                entity.Property(e => e.Zip).HasColumnName("zip");
            });

            modelBuilder.Entity<MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey });

                entity.ToTable("__MigrationHistory");

                entity.Property(e => e.MigrationId).HasMaxLength(150);

                entity.Property(e => e.ContextKey).HasMaxLength(300);

                entity.Property(e => e.Model).IsRequired();

                entity.Property(e => e.ProductVersion)
                    .IsRequired()
                    .HasMaxLength(32);
            });
        }
    }
}

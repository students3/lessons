﻿using System;
using System.Collections.Generic;

namespace Lesson19.Core.Models
{
    public partial class CourseLecturers
    {
        public string CourseId { get; set; }
        public string LcId { get; set; }
        public short LcOrder { get; set; }
        public decimal Share { get; set; }

        public Courses Course { get; set; }
        public Lecturers Lc { get; set; }
    }
}

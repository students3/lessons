﻿using System;
using System.Collections.Generic;

namespace Lesson19.Core.Models
{
    public partial class Email
    {
        public int EmId { get; set; }
        public string EmValue { get; set; }
        public string LcId { get; set; }

        public Lecturers Lc { get; set; }
    }
}

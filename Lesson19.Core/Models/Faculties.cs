﻿using System;
using System.Collections.Generic;

namespace Lesson19.Core.Models
{
    public partial class Faculties
    {
        public Faculties()
        {
            Courses = new HashSet<Courses>();
        }

        public string FaclId { get; set; }
        public string FaclName { get; set; }
        public string University { get; set; }
        public string State { get; set; }
        public string Country { get; set; }

        public ICollection<Courses> Courses { get; set; }
    }
}

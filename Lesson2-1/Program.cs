﻿using Business;
using Business.Interfaces;
using System;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1) declare variables of all simple types:
            //bool, char, int, long, decimal, double
            //byte, sbyte, short, ushort, ulong, float, uint

            bool isChecked = true;
            char f = 'x';
            string temp = "We have 5 apples";
            //var res = temp.ToCharArray().Any(x => char.IsDigit(x));
            int x1 = 12;
            decimal d = 25.75m;
            //Single
            //float
            //double

            //int y1 = x1 * (int)d;
            //string output = ((int)d).ToString();
            //string output = ((decimal)x1).ToString("N2");

            //byte b = 255;
            //UInt64 x2 = 800000000000;

            string[] fruits = { "Apple", "Banana", "Grape" };

            for (int i = 0; i < fruits.Length; i++)
            {
                Console.WriteLine(fruits[i]);
            }

            int y = 87 - 98;
            byte t = 22 << 0;

            string result = fruits.Length > 5 ? "Length is more than five" : "Length is less than five";

            //if (fruits.Length > 5)
            //{
            //    result = "Length is more than five";
            //}
            //else
            //{
            //    result = "Length is less than five";
            //}

            //int s = 5;
            //bool flag = s is int;

            //object product = new Cake();
            //flag = product is Cake;
            //flag = product is IProduct;
            //var manager = product as IManager;

            //int x3 = 10;
            IProduct product1 = new Cake();
            IProduct product2 = product1;

            if (product1 is IProduct)
            {
                var pr = product1 as IProduct;
            }

            System.Data.DataTable dtInput = new System.Data.DataTable();

            if (dtInput != null && dtInput.Rows.Count > 0)
            {
                Console.WriteLine("There are " + dtInput.Rows.Count + " rows");
            }
            else
            {
                Console.WriteLine("There are no rows in the table");
            }

            // OrElse   ||
            // AndAlso  &&
            string message = dtInput != null && dtInput.Rows.Count > 0
                                                 ? "There are " + dtInput.Rows.Count + " rows"
                                                 : "There are no rows in the table";
            Console.WriteLine(message);

            string message2 = null;
            string resp = message2 == null ? string.Empty : message2;
            string resp2 = message2 ?? string.Empty;

            Console.ReadLine();
        }
    }
}

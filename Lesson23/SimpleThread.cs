﻿using System;
using System.Linq;
using System.Threading;

namespace Lesson23
{
    public class SimpleThread
    {
        public void Run()
        {
            var args = new[] { "DFDWERT56", "mike@gmail.com" };

            var starter = new ThreadStart(Calc);                        // no arguments
            var starter2 = new ParameterizedThreadStart(CalculateSum);  // with arguments

            var thread = new Thread(CalculateSum) { IsBackground = true };
            thread.Start(args);
            Console.WriteLine("Thread started !");
        }

        private void CalculateSum(object data)
        {
            var argsThread = data as string[];
            if (argsThread != null)
            {
                Console.WriteLine($"OrderNumber: {argsThread[0]}");
                Console.WriteLine($"Email: {argsThread[1]}");
            }

            System.Threading.Thread.Sleep(3000);

            var input = new[] { 1, 2, 3, 4, 5, 6, 7 };
            var sum = input.Sum();
            Console.WriteLine($"Sum is: {sum}");
        }

        private void Calc()
        {

        }
    }
}

﻿using System;
using System.Linq;

namespace Lesson23
{
    public class SimpleThreadWait
    {
        private System.Threading.Thread _thread;
        private bool _isRunning;
        private int _sum;
        private bool _cancelled;

        public bool IsRunning
        {
            get { return _isRunning; }
        }

        public int Sum
        {
            get { return _sum; }
        }

        public void Run()
        {
            _thread = new System.Threading.Thread(CalculateSum) { IsBackground = true };
            _thread.Start();
            _isRunning = true;

            Console.WriteLine($"Thread Id: {_thread.ManagedThreadId}");
            Console.WriteLine("Thread started !");
        }

        public void Cancel()
        {
            _cancelled = true;
        }

        public void Abort()
        {
            _thread?.Abort();
            _isRunning = false;
        }

        private void CalculateSum()
        {
            for (int i = 1; i < 3; i++)
            {
                if (_cancelled)
                {
                    _isRunning = false;
                    return;
                }

                System.Threading.Thread.Sleep(1000);
            }

            var input = new[] { 1, 2, 3, 4, 5, 6, 7 };
            _sum = input.Sum();

            _isRunning = false;
        }
    }
}

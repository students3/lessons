﻿using System;
using System.Threading;
using Lesson23.ThreadPool;

namespace Lesson23
{
    class Program
    {
        static void Main(string[] args)
        {
            //var consApp = System.Threading.Thread.CurrentThread.ManagedThreadId;
            //Console.WriteLine($"Console app Thread Id: {consApp}");

            //SimpleThreadTest();
            //ThreadTestWait();
            //ThreadJoin();
            HarvestTime();


            Console.WriteLine("Press any key ...");
            Console.ReadLine();
        }

        private static void SimpleThreadTest()
        {
            var runner = new SimpleThread();
            runner.Run();
        }

        private static void ThreadTestWait()
        {
            var runner = new SimpleThreadWait();
            runner.Run();

            while (runner.IsRunning)
            {
                System.Threading.Thread.Sleep(1000);
                runner.Cancel();
                //runner.Abort();
            }

            Console.WriteLine($"Sum is: {runner.Sum}");
        }

        private static void ThreadJoin()
        {
            var runner = new ThreadsJoin();
            runner.Run();
            Console.WriteLine($"Sum is: {runner.Sum}");
        }

        private static void HarvestTime()
        {
            var harvest = new HarvestTime();
            harvest.DoWork();
        }
    }
}

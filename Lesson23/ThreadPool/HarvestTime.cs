﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lesson23.ThreadPool
{
    public class HarvestTime
    {
        public void DoWork()
        {
            var listFields = new List<GrainField>();
            var listHarvesters = new List<Harvester>(new[] {
                    new Harvester {Id = 1, HarvestSpeed = 1000},
                    new Harvester {Id = 2, HarvestSpeed = 2000},
                    new Harvester {Id = 3, HarvestSpeed = 1500},
                    new Harvester {Id = 4, HarvestSpeed = 1200},
                    new Harvester {Id = 5, HarvestSpeed = 800}
                });

            // create fields
            var rnd = new Random();

            for (int i = 0; i < 100; i++)
            {
                var grainField = new GrainField
                {
                    Id = i + 1,
                    Scissorred = false,
                    GrainQty = rnd.Next(5000, 9000)
                };

                listFields.Add(grainField);
            }

            var j = 0;

            while (j < listFields.Count)
            {
                foreach (var harvester in listHarvesters)
                {
                    if (!harvester.Busy)
                    {
                        if (j < listFields.Count)
                        {
                            var field = listFields[j];
                            harvester.CollectGrain(field);
                        }

                        j++;
                    }
                }

                System.Threading.Thread.Sleep(1000);
            }

            // wait all harvesters to finish
            while (true)
            {
                var busyHarvester = listHarvesters.FirstOrDefault(x => x.Busy);

                // if there's no busy harvester => exit loop
                if (busyHarvester == null)
                {
                    break;
                }
                else
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
        }
    }
}

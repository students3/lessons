﻿namespace Lesson23.ThreadPool
{
    public class GrainField
    {
        public int Id { get; set; }

        public bool Scissorred { get; set; }

        public int GrainQty { get; set; }
    }
}

﻿using System;
using System.Threading;

namespace Lesson23.ThreadPool
{
    public class Harvester
    {
        private int _grainQty;
        private bool _busy;
        private Thread _thread;

        public int Id { get; set; }

        public int HarvestSpeed { get; set; }

        public int GrainQty
        {
            get
            {
                return _grainQty;
            }
        }

        public bool Busy
        {
            get { return _busy; }
        }

        public void CollectGrain(GrainField field)
        {
            _busy = true;
            _grainQty = 0;

            // start thread
            _thread = new Thread(DoHarvest);
            _thread.Start(field);
        }

        private void DoHarvest(object args)
        {
            var field = args as GrainField;

            if (field != null)
            {
                int seconds = field.GrainQty / this.HarvestSpeed;
                Thread.Sleep(seconds * 1000);       // some operation

                field.Scissorred = true;
                _grainQty = field.GrainQty;

                Console.WriteLine($"Harvester: {Id} is done with field: {field.Id}, collected grain: {field.GrainQty.ToString("N2")} kgs");
            }

            _busy = false;
        }
    }
}

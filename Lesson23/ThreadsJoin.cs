﻿using System;
using System.Linq;

namespace Lesson23
{
    public class ThreadsJoin
    {
        private int _sum;

        public int Sum
        {
            get { return _sum; }
        }

        public void Run()
        {
            var thread = new System.Threading.Thread(CalculateSum) { IsBackground = true };
            thread.Start();
            Console.WriteLine("Thread started !");
            thread.Join();
        }

        private void CalculateSum()
        {
            System.Threading.Thread.Sleep(3000);

            var input = new[] { 1, 2, 3, 4, 5, 6, 7 };
            _sum = input.Sum();
        }
    }
}

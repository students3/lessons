﻿using System.Threading;

namespace Lesson23
{
    public class BackgroundWorker
    {
        private System.Threading.Thread _myThread;

        public bool ReportsProgress { get; set; }
        public bool SupportCancellation { get; set; }

        // Event ReportProgress
        // Event ProcessingCompleted

        // Delegate for method to call in another thread

        public void RunWorkerAsync()
        {
            // do smth
            _myThread = new Thread(DoSome);
        }

        private void DoSome()
        {

        }
    }
}

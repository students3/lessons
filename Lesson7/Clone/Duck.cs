﻿using System;

namespace Lesson7.Clone
{
    public class Duck : ICloneable
    {
        // shallow
        public string Name { get; set; }
        public int Age { get; set; }

        // deep: public TaskProgressChanged Task { get; set; }
        
        public object Clone()
        {
            // 1) MemberwiseClone : shallow
            return this.MemberwiseClone();

            // 2) Create new instance : deep
            //return new Duck()
            //{
            //    Age = this.Age,
            //    Name = this.Name
            //};

            // 3) Serialize object to string and recreate : deep
        }

        public override string ToString()
        {
            return "Hi I'm duck in age of " + this.Age;
        }

        public static explicit operator Duck(BaseTaskChanged v)
        {
            throw new NotImplementedException();
        }
    }
}

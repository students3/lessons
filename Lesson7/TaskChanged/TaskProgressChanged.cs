﻿using Lesson7.TaskChanged;

namespace Lesson7
{
    public class TaskProgressChanged : BaseTaskChanged
    {
        public int OldValue { get; set; }
        public int NewValue { get; set; }
        public string UserChanged { get; set; }
        public TaskProgressChangedBody Body { get; set; }

        //override
        public override string SayBye()
        {
            return "Bye from TaskProgressChanged on projectId: " + base.ProjectId;
        }

        // shadow
        public new string SayHello()
        {
            return "Hello from TaskProgressChanged on projectId: " + base.ProjectId;
        }

        public int GetOldValue()
        {
            //int OldValue = 98;

            return this.OldValue;
        }

        public string Print()
        {
            var printer = new PrintManager();
            return printer.Print(this);
        }
        
        public class TaskProgressChangedBody
        {
            public int OldValue { get; set; }
            public int NewValue { get; set; }
            public string UserChanged { get; set; }
        }
    }
}

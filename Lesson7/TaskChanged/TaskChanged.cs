﻿using System;

namespace Lesson7
{
    public class BaseTaskChanged
    {
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public DateTime Created { get; set; }
        public object RawBody { get; set; }

        public string SayHello()
        {
            return string.Empty;
        }

        public virtual string SayBye()
        {
            return string.Empty;
        }
    }
}

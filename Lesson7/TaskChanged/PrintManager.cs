﻿namespace Lesson7.TaskChanged
{
    public class PrintManager
    {
        public string Print(TaskProgressChanged taskProgressChanged)
        {
            return "ProjectId: " + taskProgressChanged.ProjectId + "\n" + 
                   "TaskId: " + taskProgressChanged.TaskId;
        }
    }
}

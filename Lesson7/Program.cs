﻿using Lesson7.Clone;
using Lesson7.Exceptions;
using Lesson7.Solid;
using System;

namespace Lesson7
{
    class Program
    {
        static void Main(string[] args)
        {
            TestException2();
            //CloneTest();
            Console.ReadLine();
        }

        private static void CloneTest()
        {
            Duck duck = new Duck()
            {
                Age = 1,
                Name = "Scrudge"
            };
            Duck duck2 = duck.Clone() as Duck;

            //Console.WriteLine(duck == duck2);

            Console.WriteLine(duck.ToString());
        }

        private static void TaskChanged()
        {
            var taskProgressChanged = new TaskProgressChanged
            {
                TaskId = 1111,
                ProjectId = 2562,
                Created = DateTime.Now.AddDays(-2),
                RawBody = new TaskProgressChanged.TaskProgressChangedBody
                {
                    OldValue = 10,
                    NewValue = 75,
                    UserChanged = "Adi Slush"
                }
            };

            var baseTask = taskProgressChanged as BaseTaskChanged;

            var result = taskProgressChanged.Print();
            Console.WriteLine(result);
        }

        private static void TestException()
        {
            //var j = 0;
            //var i = 78 / j;

            int age = 0;

            //age = int.Parse("apple", System.Globalization.NumberStyles.AllowThousands);
            //age = int.Parse(null);
            //age = int.Parse("565565565655565565");
            var b = long.Parse("565565565655565565");

            int age2 = 0;
            //int.TryParse("565565565655565565", out age);

            var duck = new Duck();
            var another = (WildDuck)duck;    // unsafe cast
            var another4 = duck as WildDuck; // safe cast
                                             //duck.ForestName = "Matveevka forest";

            var duck2 = new WildDuck();
            var another2 = (Duck)duck;
            var another3 = duck as Duck; // safe cast
        }

        private static void TestException2()
        {
            var j = 0;
            var i = 78;
            var result = 0;

            try
            {
                result = i / j;
                Console.WriteLine(result);
            }
            catch (DivideByZeroException)
            {
                //Console.WriteLine("Can't divide by zero !");
                throw new BelokonTestException("Can't divide by zero !");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}

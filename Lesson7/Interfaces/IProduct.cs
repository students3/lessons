﻿namespace Lesson7.Interfaces
{
    public interface IProduct : IProductName
    {
        decimal Price { get; set; }
    }
}

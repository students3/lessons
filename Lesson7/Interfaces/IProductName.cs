﻿namespace Lesson7.Interfaces
{
    public interface IProductName
    {
        string ProductName { get; set; }
    }
}

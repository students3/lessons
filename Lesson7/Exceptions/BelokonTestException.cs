﻿using System;

namespace Lesson7.Exceptions
{
    public class BelokonTestException : Exception
    {
        public string MyMessage { get; private set; }

        public BelokonTestException(string message)
        {
            this.MyMessage = message;
        }
    }
}

﻿namespace Lesson7.Solid
{
    public sealed class ChocolateCake : Cake
    {
        public int Calories { get; set; }
    }
}

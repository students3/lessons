﻿namespace Lesson7.Solid
{
    public class Cake
    {
        public int Price { get; set; }
        public string Name { get; set; }
    }
}

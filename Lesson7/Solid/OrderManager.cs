﻿namespace Lesson7.Solid
{
    // закрыт для изменения, но открыт для расширения
    public sealed class OrderManager
    {
        public void SaveOrder(Order order, IOrderProvider provider)
        {
            provider.SaveOrder(order);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7.Solid
{
    public interface IOrderProvider
    {
        void SaveOrder(Order order);
    }
}

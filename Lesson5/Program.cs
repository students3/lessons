﻿using Lesson5.Models.Extensions;
using System;

namespace Lesson5
{
    class Program
    {
        static void Main(string[] args)
        {
            //AnimalTest();
            //GetDistance();
            //ParamsKeyword();
            //NamedParameters();
            //CtorOverload();

         
          
            Console.ReadLine();
        }

        private static void Extensions()
        {
            string vegetable = "potato";
            int l = vegetable.MyLength();

            Console.WriteLine($"My length is: {l}");
        }

        private static void CtorOverload()
        {
            var animal1 = new Animal("murchik");
            var animal2 = new Animal();

            Console.WriteLine($"Animal1 name is: {animal1.AnimalName}");
            Console.WriteLine($"Animal2 name is: {animal2.AnimalName}");
        }

        private static void NamedParameters()
        {
            var animal = new Animal();
            var info = animal.GetDistance(time: 10, speed: 5);
            Console.WriteLine("Distance is: " + info.Distance);
            Console.WriteLine("Average speed is: " + info.AvgSpeed);
        }

        private static void ParamsKeyword()
        {
            var animal = new Animal();
            var avgSpeed = animal.GetAgvSpeed(5, 7, 8, 4, 6);
            //var avgSpeed = animal.GetAgvSpeed(new int[] { 5, 7, 8, 4, 6 });

            //Console.WriteLine("Average speed is: " + avgSpeed);
            //Console.WriteLine(string.Format("Average speed is: {0}", avgSpeed));
            Console.WriteLine($"Average speed is: {avgSpeed.ToString("N2")}");
        }

        private static void DecimalByRef()
        {
            var animal = new Animal();
            double speed = 10.5;
            animal.CorrectSpeed(ref speed);

            Console.WriteLine("Corrected speed is: " + speed);
        }

        private static void GetDistance()
        {
            var animal = new Animal();
            int speed = 10;
            int seconds = 50;
            int distance;
            double avgSpeed;

            animal.GetDistance(speed, seconds, out distance, out avgSpeed);
            Console.WriteLine("Distance is: " + distance);
            Console.WriteLine("Average speed is: " + avgSpeed);

            Console.WriteLine("\n\n\n");

            var info = animal.GetDistance(speed, seconds);
            Console.WriteLine("Distance is: " + info.Distance);
            Console.WriteLine("Average speed is: " + info.AvgSpeed);
        }

        private static void AnimalTest()
        {
            var animal = new Animal("murchik")
            {
                AnimalName = "Murchik"
            };
            animal.Feed("Fish");

            animal.Play();
            animal.Play("in the park");
            animal.Play("in the park", "ball");
            animal.Play(5);

            // out parameters
            string temp = "123";
            string temp2 = "potato";


            Console.WriteLine("\n\n\n");

            int j = 0;
            if (animal.TryParseAge(temp2, out j))
            {
                Console.WriteLine("Temp variable integer value is: " + j);
            }
            else
            {
                Console.WriteLine("Temp string variable is not integer");
            }
        }
    }
}

﻿namespace Lesson5.Models.Extensions
{
    public static class Extensions
    {
        public static int MyLength(this string input)
        {
            return !string.IsNullOrEmpty(input) ? input.Length : 0;
            //return input?.Length ?? 0;
        }
    }
}

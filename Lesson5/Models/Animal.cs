﻿using System;
using System.Linq;

namespace Lesson5
{
    public sealed class Animal
    {
        public string AnimalName { get; set; }
        public int Age { get; set; }

        #region Ctor

        public Animal(): this("Default name")
        {
        }

        public Animal(string animalName)
        {
            if (animalName == null) throw new ArgumentException("Name cannot be null !");
            this.AnimalName = animalName;
        }

        #endregion Ctor

        public void Feed(string foodName)
        {
            Console.WriteLine("Animal eats: " + foodName);
        }

        #region Play

        public void Play()
        {
            Console.WriteLine("Animal plays with us at home");
        }

        public void Play(string location)
        {
            Console.WriteLine("Animal plays with us: " + location);
        }

        public void Play(string location, string toy)
        {
            Console.WriteLine("Animal plays with us: " + location + " with " + toy);
        }

        public void Play(int age)
        {
            Console.WriteLine("Animal plays with us. His age is " + age);
        }

        #endregion Play

        public bool TryParseAge(string input, out int age)
        {
            bool result = false;
            age = 0;
            result = int.TryParse(input, out age);

            //result = input.ToCharArray().All(x => char.IsDigit(x));
            //if (result) age = int.Parse(input);

            return result;
        }

        public void GetDistance(int speed, int time, out int distance, out double avgSpeed)
        {
            distance = speed * time;
            avgSpeed = 1;
        }

        public AnimalInfo GetDistance(int speed, int time)
        {
            return new AnimalInfo()
            {
                Distance = speed * time,
                AvgSpeed = 1
            };
        }

        public void CorrectSpeed(ref double speed)
        {
            speed = Math.Round(speed, 0);
        }

        public double GetAgvSpeed(params int[] speed)
        {
            double result = 0;

            if (speed.Length > 0)
            {
                result = speed.Sum() / speed.Length;
            }

            return result;
        }

        //public double CorrectSpeed(double speed)
        //{
        //    return Math.Round(speed, 0);
        //}

        //public AnimalInfo GetDistance(int speed, int time = 1)
        //{
        //    return new AnimalInfo()
        //    {
        //        Distance = speed * time,
        //        AvgSpeed = 1
        //    };
        //}
    }
}

﻿namespace Lesson5
{
    public struct AnimalInfo
    {
        public int Distance { get; set; }
        public double AvgSpeed { get; set; }
    }
}

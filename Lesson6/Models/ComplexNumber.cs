﻿namespace Lesson6.Models
{
    public struct ComplexNumber
    {
        public int RealPart { get; set; }
        public int ImaginaryPart { get; set; }

        public static ComplexNumber operator +(ComplexNumber number1, ComplexNumber number2)
        {
            return new ComplexNumber()
            {
                RealPart = number1.RealPart + number2.RealPart,
                ImaginaryPart = number1.ImaginaryPart + number2.ImaginaryPart
            };
        }

        public static ComplexNumber operator -(ComplexNumber number1, ComplexNumber number2)
        {
            return new ComplexNumber()
            {
                RealPart = number1.RealPart - number2.RealPart,
                ImaginaryPart = number1.ImaginaryPart - number2.ImaginaryPart
            };
        }

        public static bool operator >(ComplexNumber number1, ComplexNumber number2)
        {
            return (number1.RealPart > number2.RealPart) &&
                   (number1.ImaginaryPart > number2.ImaginaryPart);
        }

        public static bool operator <(ComplexNumber number1, ComplexNumber number2)
        {
            return (number1.RealPart < number2.RealPart) &&
                   (number1.ImaginaryPart < number2.ImaginaryPart);
        }

        public static bool operator ==(ComplexNumber number1, ComplexNumber number2)
        {
            return (number1.RealPart == number2.RealPart) &&
                   (number1.ImaginaryPart == number2.ImaginaryPart);
        }

        public static bool operator !=(ComplexNumber number1, ComplexNumber number2)
        {
            return (number1.RealPart != number2.RealPart) ||
                   (number1.ImaginaryPart != number2.ImaginaryPart);
        }

        public static implicit operator int(ComplexNumber number)
        {
            return number.RealPart;
        }

        public static explicit operator double(ComplexNumber number)
        {
            return number.RealPart;
        }


        public override int GetHashCode()
        {
            var code = this.RealPart.GetHashCode();
            return  code;
        }

        public override bool Equals(object obj)
        {
            var result = false;

            if (obj is ComplexNumber)
            {
                var number2 = (ComplexNumber)obj;
                result = (this.RealPart == number2.RealPart) &&
                         (this.ImaginaryPart == number2.ImaginaryPart);
            }
            
            return result;
        }
    }
}

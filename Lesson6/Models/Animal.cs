﻿namespace Lesson6
{
    public sealed class Animal
    {
        public string AnimalName { get; set; }
        public int Age { get; set; }

        public static int operator +(Animal animal1, Animal animal2)
        {
            return animal1.Age + animal2.Age;
        }
    }
}

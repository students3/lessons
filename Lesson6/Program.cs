﻿using Lesson6.Models;
using System;
using System.Linq;

namespace Lesson6
{
    class Program
    {
        static void Main(string[] args)
        {
            //OperatorAdd();
            ComplexNumbers();
            
            //string s1 = "apple";
            //string s2 = "Apple";

            //var eq = s1 == s2;
            //var eq2 = string.Equals(s1, s2, StringComparison.InvariantCultureIgnoreCase);
                       
            Console.ReadLine();
        }

        private static void OperatorAdd()
        {
            var animal1 = new Animal()
            {
                Age = 2
            };
            var animal2 = new Animal()
            {
                Age = 4
            };

            var sumAge = animal1 + animal2;
            Console.WriteLine($"Sum age is: {sumAge}");

            var diff = animal1.Age - animal2.Age;
        }

        private static void ComplexNumbers()
        {
            var number1 = new ComplexNumber()
            {
                RealPart = 15,
                ImaginaryPart = 1
            };

            var number2 = new ComplexNumber()
            {
                RealPart = 15,
                ImaginaryPart = 2
            };

            //var b1 = number1 > number2;
            //var b2 = number1 < number2;

            //var x1 = number1 + number2;
            //var x2 = number1 - number2;

            var y1 = number1 == number2;

            var arrTemp = new ComplexNumber[] { number1, number2 };
            arrTemp = arrTemp.Distinct().ToArray();

            // TODO: overload for operator: true, false
            // TODO: overload for operator: ++
            //if (number1)
            //{

            //}

            var x1 = (int)number1;
            //var x2 = number1 as double?;

            Console.WriteLine("Finished");
        }

        private static void FindArray()
        {
            string[] fruits = new string[] { "apple", "banana", "pear", "grape" };
            string input = "PEAR";
            var index = FindInArray(fruits, input);

            Console.WriteLine($"Found {input} on index {index}");
        }

        private static int FindInArray(string[] fruits, string fruitName)
        {
            int result = -1;

            for (var i = 0; i < fruits.Length; i++)
            {
                if (string.Equals(fruits[i], fruitName, StringComparison.InvariantCultureIgnoreCase))
                {
                    result = i;
                    break;
                }
            }

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson13
{
    public static class Extensions
    {
        public static bool Contains(this string input, string search, bool ignoreCase)
        {
            bool result = false;

            if (ignoreCase)
            {
                // our logic
                if (search != null && input != null)
                {
                    result = input.ToLower().Contains(search.ToLower());
                }
            }
            else
            {
                result = input.Contains(search);
            }

            return result;
        }
    }
}

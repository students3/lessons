﻿namespace Lesson13
{
    class Distance
    {
        private decimal _distance;

        public Distance(decimal distance)
        {
            this._distance = distance;
        }

        public override string ToString()
        {
            return $"{_distance.ToString("N1")} km";
        }
    }
}

﻿using System;
using System.Globalization;
using System.Text;
using Reg = System.Text.RegularExpressions;

namespace Lesson13
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestEquals();
            //TestStringContains();

            //string result = TestStringFormat("Vasya", "bought", "some milk", "at supermarket", "at nine o'clock");
            //Console.WriteLine(result);

            //result = TestInterpolation("Vasya", "bought", "some milk", "at supermarket", "at nine o'clock");
            //Console.WriteLine(result);

            //string name = "Vasya".Remove(2);
            //Console.WriteLine(name);

            //TestPad();
            //TestSplit();
            //TestTrim();
            //TestInitialize();
            //TestToString();

            //TestStringBuilder();
            TestRegEx();

            Console.ReadLine();
        }

        private static void TestStringContains()
        {
            var input = "Vasya bought some milk";
            var search = "Milk";

            var result = input.Contains(search, true);
            Console.WriteLine(result ? "Contains" : "There's no " + search + " inside");
        }

        private static void TestEquals()
        {
            var input = "Milk";
            var input2 = input;

            var input3 = string.Copy(input);

            input2 = "Milk98";

            //string.Intern("Milk");

            Console.WriteLine(input.Equals(input2) ? "Equals" : "Not equal");
            Console.WriteLine(input.Equals(input3) ? "Equals" : "Not equal");
        }

        private static string TestStringFormat(string name, string action, string product, string location, string time)
        {
            // Vasya bought some milk at supermarket at nine o'clock";
            string template = "{0} {1} {2} {3} {4}";
            string result = string.Format(template, name, action, product, location, time);

            return result;
        }

        private static string TestInterpolation(string name, string action, string product, string location, string time)
        {
            // Vasya bought some milk at supermarket at nine o'clock";
            string result = $"{name} {action} {product} {location} {time}";

            return result;
        }

        private static void TestSplit()
        {
            // Important !!!
            string input = "Vasya     bought     some      milk";
            var nameParts = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var result = string.Join(" ", nameParts);

            var html = "<li>some text1</li><li>some text2</li>";
            var parts = html.Split(new string[] { "<li>", "</li>" }, StringSplitOptions.RemoveEmptyEntries);

            var b = 0;
        }

        private static void TestPad()
        {
            string s1 = "Vasya is going home";
            string s2 = "Vasya";
            string s3 = "Vasya is";

            s2 = s2.PadRight(s1.Length);
            s3 = s3.PadRight(s1.Length);

            Console.WriteLine($"'{s2}'");
            Console.WriteLine($"'{s3}'");
        }

        private static void TestTrim()
        {
            string s1 = "Vasya is going home   /  /";
            s1 = s1.TrimEnd(new char[] { ' ', '/' });
            Console.WriteLine($"'{s1}'");
        }

        private static void TestInitialize()
        {
            string input = @"d:\02 Cloud\JustManage\";
            var b = 0;
            Console.WriteLine(input);
        }

        private static void TestToString()
        {
            // IFormatProvider myProvider = new IFormatProvider

            var number = 5789.562;
            Console.WriteLine(number.ToString("N4"));
            Console.WriteLine(number.ToString("C"));

            var percents = 0.75;
            Console.WriteLine(percents.ToString("p0"));

            var now = DateTime.Now;
            Console.WriteLine(now.ToString("g"));
        }

        private static void TestDistance()
        {
            var input = 75.89m;
            var distance = new Distance(input);
            Console.WriteLine(distance.ToString());
        }

        private static void TestStringBuilder()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Hello Sasha");                   // "Hello Sasha" + "\r\n"
            sb.AppendFormat("{0} {1}", "Hello", "world");   // "Hello world"
            sb.Append("!");                                 // "!"

            Console.WriteLine(sb.ToString());

            sb.Clear();
            sb = null;
        }

        private static void TestRegEx()
        {
            //    ^[\w| ]{1,}$   =>   hello word again
            //    ^[\w|\s|\d|-]{1,}$
            var regex = new Reg.Regex(@"\d+", Reg.RegexOptions.IgnoreCase);
            var input = "Мама купила в 2 магащинах 10 кг яблок";

            if (regex.IsMatch(input))
            {
                Reg.MatchCollection matches = regex.Matches(input);

                foreach (Reg.Match itm in matches)
                {
                    Console.WriteLine($"Position: {itm.Index}, Value: {itm.Value}");
                }
            }
            else
            {
                Console.WriteLine("Matches not found");
            }
        }

        private static void Globalization()
        {
            decimal val = 1603.42m;
            double v = 1043.62957;

            Console.WriteLine(val.ToString("C3", new CultureInfo("en-US")));
            Console.WriteLine(val.ToString("C3", new CultureInfo("fr")));
            Console.WriteLine(val.ToString("C3", new CultureInfo("de")));

            string[] cultureNames = { "en-US", "en-GB", "ru", "fr", "de" };

            foreach (var name in cultureNames)
            {
                NumberFormatInfo nfi = CultureInfo.CreateSpecificCulture(name).NumberFormat;
                Console.WriteLine("{0,-6} {1}", name + ":", v.ToString("N3", nfi));
            }

            Console.WriteLine();

            foreach (var name in cultureNames)
            {
                DateTimeFormatInfo dfi = CultureInfo.CreateSpecificCulture(name).DateTimeFormat;
                Console.WriteLine("{0,-6} {1}", name + ":", DateTime.Now.ToString("D", dfi));
            }

            // This example displays the following output to the console:
            //       d: 6/15/2008
            //       D: Sunday, June 15, 2008
            //       f: Sunday, June 15, 2008 9:15 PM
            //       F: Sunday, June 15, 2008 9:15:07 PM
            //       g: 6/15/2008 9:15 PM
            //       G: 6/15/2008 9:15:07 PM
            //       m: June 15
            //       o: 2008-06-15T21:15:07.0000000
            //       R: Sun, 15 Jun 2008 21:15:07 GMT
            //       s: 2008-06-15T21:15:07
            //       t: 9:15 PM
            //       T: 9:15:07 PM
            //       u: 2008-06-15 21:15:07Z
            //       U: Monday, June 16, 2008 4:15:07 AM
            //       y: June, 2008

            Console.WriteLine();
        }
    }
}

﻿CREATE TABLE [dbo].[UserRoles] (
    [UserRoleId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]     INT NOT NULL,
    [RoleId]     INT NOT NULL,
    CONSTRAINT [PK_UserRoles_UserRoleId] PRIMARY KEY CLUSTERED ([UserRoleId] ASC)
);


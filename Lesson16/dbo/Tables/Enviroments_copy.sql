﻿CREATE TABLE [dbo].[Enviroments_copy] (
    [EnviromentId]   INT          IDENTITY (1, 1) NOT NULL,
    [EnviromentName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Enviroments_EnviromentId_copy] PRIMARY KEY CLUSTERED ([EnviromentId] ASC)
);


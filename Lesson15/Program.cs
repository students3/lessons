﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Lesson15
{
    class Program
    {
        static void Main(string[] args)
        {
            TestSerialization();
            NewtonJson();

            Console.WriteLine("Finished");
            Console.ReadLine();
        }

        private static void TestSerialization()
        {
            var binFormatter = new BinaryFormatter();

            var profession = new Profession
            {
                ProfessionId = 1,
                ProfessionName = "Builder"
            };

            var binStream = new FileStream("MyFile.bin", FileMode.Create, FileAccess.Write, FileShare.None);
            binFormatter.Serialize(binStream, profession);

            var xmlFormatter = new XmlSerializer(typeof(Profession));
            var xmlStream = new FileStream("MyXMLFile.xml", FileMode.Create, FileAccess.Write, FileShare.None);
            xmlFormatter.Serialize(xmlStream, profession);

            binStream.Close();
            xmlStream.Dispose();
        }

        private static void NewtonJson()
        {
            var profession = new Profession
            {
                ProfessionId = 1,
                ProfessionName = "Builder"
            };
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(profession);

            using (var writer = new StreamWriter("MyJson.txt", true))
            {
                writer.WriteLine(json);
            }

            //json = "{'PersonId': 2, 'ProfessionName': 'Vasya'}";
            //var profession2 = Newtonsoft.Json.JsonConvert.DeserializeObject<Profession>(json);

            //Console.WriteLine($"ProfessionId: {profession2.ProfessionId}");
            //Console.WriteLine($"ProfessionName: {profession2.ProfessionName}");
        }
    }
}

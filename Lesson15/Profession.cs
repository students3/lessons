﻿using System;
using System.Runtime.Serialization;

namespace Lesson15
{
    [Serializable()]
    public class Profession : ISerializable
    {
        public int ProfessionId { get; set; }
        public string ProfessionName { get; set; }

        public double SalaryMin { get; set; }
        public string Manager { get; set; }

        // When serializing data from file
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("500", SalaryMin);
            info.AddValue("BigBoss", Manager);
        }

        public Profession() { }

        // When de-serializing data from file
        protected Profession(SerializationInfo info, StreamingContext context)
        {
            // if SalarMin == 0 then assign SalarMin == 500
            SalaryMin = info.GetDouble("500");
            Manager = info.GetString("BigBoss");
        }
    }
}

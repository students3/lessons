﻿using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;

namespace Lesson18
{
    class Program
    {
        static void Main(string[] args)
        {
            //GetDataSet();
            //GetDataSetFromDb();
            //TestConnStringBuilder();
            TestSqlParameters();



            Console.ReadLine();
        }

        private static DataTable GetHistoryDatatable()
        {
            var dtInput = new DataTable();
            dtInput.TableName = "History";
            dtInput.Columns.Add("OriginalColumn", typeof(System.String));
            dtInput.Columns.Add("MapTo", typeof(System.String));

            var dr = dtInput.NewRow();
            //dr["OriginalColumn"] = "Task";
            dr[0] = "Task";
            dr[1] = "TaskName";
            dtInput.Rows.Add(dr);

            dr = dtInput.NewRow();
            dr[0] = "Id";
            dr[1] = "TaskId";
            dtInput.Rows.Add(dr);

            return dtInput;
        }

        private static DataTable GetMappingDatatable()
        {
            var dtInput = new DataTable();
            dtInput.TableName = "Mapping";
            dtInput.Columns.Add("Letter", typeof(System.String));
            dtInput.Columns.Add("OriginalColumn", typeof(System.String));
            dtInput.Columns.Add("MapTo", typeof(System.String));

            var dr = dtInput.NewRow();
            dr[0] = "A";
            dr[1] = "Task";
            dr[2] = "";
            dtInput.Rows.Add(dr);

            dr = dtInput.NewRow();
            dr[0] = "B";
            dr[1] = "Id";
            dr[2] = "";
            dtInput.Rows.Add(dr);

            return dtInput;
        }

        private static void GetDataSet()
        {
            var ds = new DataSet();
            ds.Tables.Add(GetMappingDatatable());
            ds.Tables.Add(GetHistoryDatatable());
        }

        private static void GetDataSetFromDb()
        {
            string connstring = "Data Source=(local);Encrypt=False;Integrated Security=True;User ID=Andrew-PC\\Andrew";
            var adapter = new SqlDataAdapter("Select * from Lesson16.dbo.Tasks; Select * FROM Lesson16.dbo.Users", connstring);
            var personalInfoDataSet = new DataSet();
            adapter.Fill(personalInfoDataSet);

            int modified = 0;
            int added = 0;


            // 1.1) new row in Tasks
            var dtInput = personalInfoDataSet.Tables[0];
            var dr = dtInput.NewRow();
            dr["TaskName"] = "Maksym";
            dr["UserId"] = 5;
            dtInput.Rows.Add(dr);

            // 1.2) Update row in Tasks
            dr = dtInput.Rows[0];
            dr["TaskName"] = "visit Tanya";

            modified = dtInput.AsEnumerable().Count(x => x.RowState == DataRowState.Modified);
            added = dtInput.AsEnumerable().Count(x => x.RowState == DataRowState.Added);

            // 2.1) new row in Users
            dtInput = personalInfoDataSet.Tables[1];
            dr = dtInput.NewRow();
            dr["UserName"] = "Maksym";
            dtInput.Rows.Add(dr);

            // 2.2) Update row in Users
            dr = dtInput.Rows[0];
            dr["UserName"] = "Tanya";

            modified = dtInput.AsEnumerable().Count(x => x.RowState == DataRowState.Modified);
            added = dtInput.AsEnumerable().Count(x => x.RowState == DataRowState.Added);

            var cmdBuilder = new SqlCommandBuilder(adapter);
            adapter.Update(personalInfoDataSet);
        }

        private static void TestConnStringBuilder()
        {
            var buider = new SqlConnectionStringBuilder();
            buider.DataSource = "(local)";
            buider.InitialCatalog = "Lesson16";
            buider.IntegratedSecurity = true;
            buider.UserInstance = true;

            var connString = buider.ToString();
            Console.WriteLine(connString);
        }

        private static void TestSqlParameters()
        {
            // 1. declare command object with parameter
            string connstring = "Data Source=(local);Encrypt=False;Integrated Security=True;User ID=Andrew-PC\\Andrew";
            var conn = new SqlConnection(connstring);
            var cmd = new SqlCommand("select * from Lesson16.dbo.Users where UserName = @userName", conn);
            var param = new SqlParameter()
            {
                ParameterName = "@userName",
                Value = "Tanya"
            };

            // 3. add new parameter to command object
            cmd.Parameters.Add(param);

            // get data stream
            conn.Open();
            var reader = cmd.ExecuteReader();

            // write each record
            while (reader.Read())
            {
                Console.WriteLine("{0}, {1}", reader["UserId"], reader["UserName"]);
            }

            conn.Close();

            //string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
        }

        private static void TestTransactions()
        {
            string connstring = "Data Source=(local);Encrypt=False;Integrated Security=True;User ID=Andrew-PC\\Andrew";

            using (var connection = new SqlConnection(connstring))
            {
                connection.Open();

                var tr = connection.BeginTransaction();
                var command = connection.CreateCommand();
                command.Transaction = tr;

                try
                {
                    command.CommandText = "INSERT INTO email VALUES(5, 'user5@example.com', 'L_5'); ";
                    command.ExecuteNonQuery();
                    command.CommandText = "INSERT INTO email VALUES(6, 'user6@example.com', 'L_6'); ";
                    command.ExecuteNonQuery();
                    tr.Commit();

                    Console.WriteLine("Ok.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                    try
                    {
                        tr.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        Console.WriteLine(exRollback.Message);
                    }
                }
            }
        }
    }
}

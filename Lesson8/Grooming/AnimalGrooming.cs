﻿using Lesson8.Grooming;
using Lesson8.Grooming.Beverage;

namespace Lesson8.Models
{
    public abstract class AnimalGrooming<T> : IGrooming<T> where T : IAnimal
                                                    
    {
        public void MakeGrooming(T animal)
        {
            if (animal.Weight < 1)
            {
                System.Console.WriteLine("Sorry, we can't groom your: " + animal.Name +
                                         " Its' too small");
            }

            this.OfferBeverageToOwner(new Tea());
            System.Console.WriteLine("Making grooming for: " + animal.Name);
            this.SayAboutDiscount();
        }

        public void MakeGrooming<V, U>(T animal, V tool, U tool2) where V : ITool where U : ITool
        {
            if (animal.Weight < 1)
            {
                System.Console.WriteLine("Sorry, we can't groom your: " + animal.Name +
                                         " Its' too small");
            }

            this.OfferBeverageToOwner(new Tea());
            System.Console.WriteLine("Making grooming for: " + animal.Name +
                                    " with tool: " + tool.ToolName + " and " + tool2.ToolName);
            this.SayAboutDiscount();
        }

        private void OfferBeverageToOwner<Z>(Z tea) where Z : IBeverage
        {
            System.Console.WriteLine("Offering tea to owner");
        }

        private void SayAboutDiscount()
        {
            System.Console.WriteLine("Come to us next time and you'll get discount");
        }
    }
}

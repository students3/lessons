﻿namespace Lesson8.Grooming
{
    public class Dog : IAnimal
    {
        public string Name { get; set; }
        public int Weight { get; set; }
    }
}

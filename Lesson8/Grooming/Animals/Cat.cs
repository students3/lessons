﻿namespace Lesson8.Grooming
{
    public class Cat : IAnimal
    {
        public string Name { get; set; }
        public int Weight { get; set; }

        public Cat()
        {
            Weight = 6;
        }
    }
}

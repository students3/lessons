﻿namespace Lesson8.Grooming
{
    public interface IAnimal
    {
        string Name { get; set; }
        int Weight { get; set; }
    }
}

﻿namespace Lesson8.Grooming.Tools
{
    public class Scissors : ITool
    {
        public string ToolName { get ; private set; }

        public Scissors()
        {
            this.ToolName = "scissors";
        }
    }
}

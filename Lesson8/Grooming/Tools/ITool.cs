﻿namespace Lesson8.Grooming
{
    public interface ITool
    {
        string ToolName { get; }
    }
}

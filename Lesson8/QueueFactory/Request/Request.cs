﻿namespace Lesson8.QueueFactory
{
    public class Request
    {
        public string Email { get; set; }
        public string EntityType { get; set; } // product, category
        public string ReportType { get; set; } // pdf, xls
    }
}

﻿using System.Collections.Generic;

namespace Lesson8.QueueFactory
{
    public abstract class AbstractQueueFactory<T> where T : Request
    {
        private Queue<T> queue = new Queue<T>();

        public void AddNew(T request)
        {
            queue.Enqueue(request);
        }

        public T GetFirstRequest()
        {
            T result = null;

            if (queue.Count > 0)
            {
                result = queue.Dequeue();
            }

            return result;
        }
    }
}

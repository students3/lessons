﻿namespace Lesson8.Models
{
    public class Product
    {
        public int ProdId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}

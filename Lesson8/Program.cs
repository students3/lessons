﻿using System;
using Lesson8.Extensions;
using System.Collections.Generic;
using Lesson8.Models;
using System.Linq;
using Lesson8.Grooming;
using Lesson8.QueueFactory;
using Lesson8.Grooming.Tools;

namespace Lesson8
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestArrayExtension();
            //TestList();
            //TestArrayExtension2();
            //TestCategories();
            //ArraySort();

            GroomingTest();

            Console.ReadLine();
        }

        private static void GroomingTest()
        {
            var cat = new Cat { Name = "Barsik" };
            var catGrooming = new CatGrooming();
            catGrooming.MakeGrooming(cat, new Scissors(), new Comb());
        }

        private static void TestQueue()
        {
            var import = new ImportQueue();
            var request = new ImportRequest
            {
                Email = "vasya@gmail.com",
                EntityType = "products",
                ReportType = "xlsx"
            };
            import.AddNew(request);
        }

        private static void ArraySort()
        {
            int[] arrTemp = new int[] { 5, 8, 5465, 7, 52 };
            Array.Sort(arrTemp);
            arrTemp = arrTemp.Reverse().ToArray();

            foreach (var item in arrTemp)
            {
                Console.WriteLine(item);
            }
        }

        private static void TestIrregular()
        {
            //int[] arrTemp = new int[] { 5 , "string", 6556565656565};
            //object arrTemp = new object[] { 5, "string", 6556565656565 };          
        }

        private static void TestList()
        {
            List<string> listFruits = new List<string>();
            listFruits.AddRange(new[] { "apple", "pear", "banana", "apple", "apple" });

            listFruits.Remove("apple");
            listFruits.RemoveAll(x => x.Length > 4);

            List<int> listNumbers = new List<int>();
            listNumbers.AddRange(new[] { 5, 8, 10, 4, 25 });

            listNumbers.RemoveAll(x => x > 4);
        }

        private static void TestArrayExtension()
        {
            int[] arrTemp = new int[] { 4, 8, 5, 9, 52, 2, 5 };

            int qty5 = arrTemp.GetNumberQty(5);
            Console.WriteLine($"Qty of number 5 is: {qty5}");

            int qty4 = arrTemp.GetNumberQty(4);
            Console.WriteLine($"Qty of number 4 is: {qty4}");
        }

        private static void TestArrayExtension2()
        {
            int[] arrTemp = new int[] { 4, 8, 5, 9, 52, 2, 5 };

            int qty5 = arrTemp.GetItemQty(5);
            Console.WriteLine($"Qty of number 5 is: {qty5}");

            int qty4 = arrTemp.GetItemQty(4);
            Console.WriteLine($"Qty of number 4 is: {qty4}");

            string[] arrFruits = new string[] { "apple", "pear", "banana", "apple", "apple" };
            int qtyApples = arrFruits.GetItemQty("apple");
            Console.WriteLine($"Qty of apples is: {qtyApples}");
        }

        private static void TestDictionary()
        {
            // O(1) - 1 sec/elem => 1 sec
            Dictionary<string, string> dicEnglishWords = new Dictionary<string, string>();
            dicEnglishWords.Add("apple", "яблоко");
            dicEnglishWords.Add("pear", "груша");

            string translation = dicEnglishWords["apple"];
            
            string[,] arrTemp = new string[,] {  // O(N) - 1 sec/elem => 500 sec
                 { "apple", "яблоко" },
                 { "pear", "груша" }
            };
        }

        private static void TestCategories()
        {
            var product1 = new Product
            {
                ProdId = 12,
                CategoryId = 5
            };
            var product2 = new Product
            {
                ProdId = 13,
                CategoryId = 5
            };
            var product3 = new Product
            {
                ProdId = 13,
                CategoryId = 8
            };
            var product4 = new Product
            {
                ProdId = 13,
                CategoryId = 75
            };

            Product[] products = new Product[] { product1, product2, product3, product4 };
            HashSet<int> hsCategories = new HashSet<int>();

            foreach (var item in products)
            {
                hsCategories.Add(item.CategoryId);
            }

            // Categories assign
            Dictionary<int, string> dicCategories = new Dictionary<int, string>();
            dicCategories.Add(5, "Exhaust");
            dicCategories.Add(8, "Floor mat");

            foreach (var item in products)
            {
                if (dicCategories.ContainsKey(item.CategoryId))
                {
                    item.CategoryName = dicCategories[item.CategoryId];
                }

                Console.WriteLine($"ProductId: {item.ProdId}, " +
                                  $"CategoryId: {item.CategoryId}, " +
                                  $"CategoryName: {item.CategoryName}");
            }
        }
    }
}

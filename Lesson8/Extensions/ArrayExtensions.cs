﻿namespace Lesson8.Extensions
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Counts qty of number in array
        /// </summary>
        /// <param name="arrTemp"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int GetNumberQty(this int[] arrTemp, int number)
        {
            int result = 0;

            foreach (var item in arrTemp)
            {
                if (item == number)
                {
                    result++;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">decimal, int, string, Duck</typeparam>
        /// <param name="arrTemp"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int GetItemQty<T>(this T[] arrTemp, T number)
        {
            int result = 0;

            foreach (var item in arrTemp)
            {
                if (item.Equals(number))
                {
                    result++;
                }
            }

            return result;
        }
    }
}

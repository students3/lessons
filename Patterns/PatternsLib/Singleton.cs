﻿namespace PatternsLib
{
    public class Singleton
    {
        private static readonly object lockingObject = new object();
        private static volatile Singleton _instance;

        public static Singleton Instance()
        {
            if (_instance == null)
            {
                lock (lockingObject)
                {
                    if (_instance == null)
                    {
                        _instance = new Singleton();
                    }
                }
            }

            return _instance;
        }
    }
}

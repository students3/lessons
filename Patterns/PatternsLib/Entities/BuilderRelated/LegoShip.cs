﻿using PatternsLib.Interfaces;

namespace PatternsLib.Entities.BuilderRelated
{
    public class LegoShip : ILegoObject
    {
        public string Name => "Ship";
        public int TotalComponents => 1560;

        public IDrawing GetDrawing()
        {
            return new LegoShipDrawing();
        }
    }
}

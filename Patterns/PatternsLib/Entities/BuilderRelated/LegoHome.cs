﻿using PatternsLib.Interfaces;

namespace PatternsLib.Entities.BuilderRelated
{
    public class LegoHome : ILegoObject
    {
        public string Name => "Home";
        public int TotalComponents => 1000;

        public IDrawing GetDrawing()
        {
            return new LegoHomeDrawing();
        }
    }
}

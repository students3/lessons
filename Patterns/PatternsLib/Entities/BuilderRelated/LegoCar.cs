﻿using PatternsLib.Interfaces;

namespace PatternsLib.Entities.BuilderRelated
{
    public class LegoCar : ILegoObject
    {
        public string Name => "Car";
        public int TotalComponents => 520;

        public IDrawing GetDrawing()
        {
            return new LegoCarDrawing();
        }
    }
}

﻿using System.Collections.Generic;
using PatternsLib.Entities.BuilderRelated;
using PatternsLib.Interfaces;

namespace PatternsLib.Implementations.Builders
{
    public class HomeBuilder : IBuilder
    {
        public ILegoObject Build(List<ILegoObject> details)
        {
            var home = new LegoHome();

            // home.Read();

            // BuildInternal();

            return home;
        }
    }
}

﻿using System.Collections.Generic;
using PatternsLib.Entities.BuilderRelated;
using PatternsLib.Interfaces;

namespace PatternsLib.Implementations.Builders
{
    public class ShipBuilder : IBuilder
    {
        public ILegoObject Build(List<ILegoObject> details)
        {
            var ship = new LegoShip();

            // home.Read();

            // BuildInternal();

            return ship;
        }
    }
}
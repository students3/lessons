﻿using System.Collections.Generic;
using PatternsLib.Entities.BuilderRelated;
using PatternsLib.Interfaces;

namespace PatternsLib.Implementations.Builders
{
    public class CarBuilder : IBuilder
    {
        public ILegoObject Build(List<ILegoObject> details)
        {
            var car = new LegoCar();
            var drawing = car.GetDrawing();

            // TODO: Go though drawing and do the drawing
            // drawing.Read();

            // BuildInternal(drawing);

            return car;
        }
    }
}

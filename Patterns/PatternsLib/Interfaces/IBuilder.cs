﻿using System.Collections.Generic;

namespace PatternsLib.Interfaces
{
    public interface IBuilder
    {
        ILegoObject Build(List<ILegoObject> details);
    }
}

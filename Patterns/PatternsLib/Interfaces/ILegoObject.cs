﻿namespace PatternsLib.Interfaces
{
    public interface ILegoObject
    {
        string Name { get; }
        int TotalComponents { get; }

        IDrawing GetDrawing();
    }
}

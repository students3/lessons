﻿namespace PatternsLib.Interfaces
{
    public interface ILegoDetail
    {
        string Code { get; set; }
        int Length { get; set; }
        int Width { get; set; }
    }
}

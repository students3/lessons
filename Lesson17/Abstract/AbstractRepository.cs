﻿using Lesson17.Interfaces;

namespace Lesson17.Abstract
{
    public abstract class AbstractRepository
    {
        protected readonly string TableName;
        protected readonly string PrimaryKeyName;
        protected readonly IProvider Provider;

        protected AbstractRepository(string tableName, string primaryKeyName, IProvider provider)
        {
            this.TableName = tableName;
            this.PrimaryKeyName = primaryKeyName;
            this.Provider = provider;
        }
    }
}

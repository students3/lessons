﻿using Lesson17.Interfaces.Manager;
using Lesson17.Interfaces.Repository;
using Lesson17.Models;

namespace Lesson17.Manager
{
    public class TaskManager: ITaskManager
    {
        private readonly ITaskRepository _taskRepository;

        public TaskManager(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public UserTask GetTask(int id)
        {
            return _taskRepository.Get(id);
        }

        public int InsertTask(UserTask record)
        {
            return _taskRepository.InsertRecord(record);
        }

        public void UpdateTask(UserTask record)
        {
            _taskRepository.UpdateRecord(record);
        }

        public void DeleteTask(UserTask record)
        {
            _taskRepository.DeleteRecord(record.TaskId);
        }
    }
}

﻿using System.Data.SqlClient;

namespace Lesson17
{
    public class DbManager: IDbManager
    {
        private readonly string _connectionString;
        private readonly SqlConnection _sqlConnection;

        public DbManager(string connectionString)
        {
            _connectionString = connectionString;
            _sqlConnection = new SqlConnection(connectionString);
        }

        public void OpenConnection()
        {
            _sqlConnection.Open();
        }

        public void CloseConnection()
        {
            _sqlConnection.Close();
        }

        public SqlDataReader GetReader(string query)
        {
            SqlCommand command = new SqlCommand(query, _sqlConnection);
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public int ExecuteNonQuery(string query)
        {
            SqlCommand command = new SqlCommand(query, _sqlConnection);
            return command.ExecuteNonQuery();
        }

        ~DbManager()
        {
            if (_sqlConnection?.State == System.Data.ConnectionState.Open) _sqlConnection.Close();
            _sqlConnection?.Dispose();
        }
    }
}

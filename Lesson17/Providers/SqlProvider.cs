﻿using System;
using System.Data.SqlClient;
using Lesson17.Interfaces;

namespace Lesson17.Providers
{
    class SqlProvider : IProvider
    {
        private readonly SqlConnection _sqlConnection;

        public SqlProvider(string connString)
        {
            _sqlConnection = new SqlConnection(connString);
        }

        public void Close()
        {
            _sqlConnection.Close();
        }

        public void Open()
        {
            _sqlConnection.Open();
        }

        public object[] Get(string query)
        {
            var command = new SqlCommand(query, _sqlConnection);
            var result = new object[] { };

            try
            {
                Open();

                var reader = command.ExecuteReader();
                result = new object[reader.FieldCount];
                reader.GetValues(result);
                reader.Dispose();
            }
            catch (Exception ex)
            {
                // log error somewhere
            }
            finally
            {
                command.Dispose();
                Close();
            }

            return result;
        }

        public void ExecuteQuery(string query)
        {
            var command = new SqlCommand(query, _sqlConnection);

            try
            {
                Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // log error somewhere
            }
            finally
            {
                command.Dispose();
                Close();
            }
        }

        public string ExecuteScalar(string query)
        {
            var result = string.Empty;
            var command = new SqlCommand(query, _sqlConnection);

            try
            {
                Open();
                result = command.ExecuteScalar()?.ToString();
            }
            catch (Exception ex)
            {
                // log error somewhere
            }
            finally
            {
                command.Dispose();
                Close();
            }

            return result;
        }
    }
}

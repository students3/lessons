﻿using System;
using System.Data.SqlClient;
using Lesson17.Manager;
using Lesson17.Models;
using Lesson17.Providers;
using Lesson17.Repositories;

namespace Lesson17
{
    class Program
    {
        private static string connstring = @"Data Source=HOME-PC\SQLEXPRESS;Encrypt=False;Integrated Security=True;User ID=Home-PC\Andrew;Initial catalog=Lesson16;";

        static void Main(string[] args)
        {
            //TestSqlReader();
            //TestSqlReader2();
            //TestUpdate();

            TestDomainModel();

            Console.ReadLine();
        }

        private static void TestSqlReader()
        {
            string queryString = "Select * from dbo.Tasks";

            using (var dbconn = new SqlConnection(connstring))
            {
                dbconn.Open();

                SqlCommand command = new SqlCommand(queryString, dbconn);
                SqlDataReader reader = command.ExecuteReader();

                try
                {
                    // reader.FieldCount
                    object[] values = new object[reader.FieldCount];

                    while (reader.Read())
                    {
                        reader.GetValues(values);
                        Console.WriteLine(string.Join("\t", values));
                        //Console.WriteLine(string.Format("{0} {1} {2} {3}", reader[0], reader[1], reader[2], reader[3]));
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
        }

        private static void TestSqlReader2()
        {
            string queryString = "Select * from dbo.Tasks";
            IDbManager dbManager = new DbManager(connstring);

            dbManager.OpenConnection();
            var reader = dbManager.GetReader(queryString);

            try
            {
                while (reader.Read())
                {
                    Console.WriteLine(string.Format("{0} {1} {2} {3}", reader[0], reader[1], reader[2], reader[3]));
                }
            }
            finally
            {
                reader.Close();
            }

            dbManager.CloseConnection();
        }

        private static void TestUpdate()
        {
            string queryString = "Update dbo.Tasks " +
                                 "SET EnviromentId = 25 " +
                                 "WHERE UserId = 1 AND EnviromentId != 25";
            IDbManager dbManager = new DbManager(connstring);

            dbManager.OpenConnection();
            int rowsAffected = dbManager.ExecuteNonQuery(queryString);
            dbManager.CloseConnection();

            Console.WriteLine($"Finished. Rows affected: {rowsAffected}");
        }

        private static void TestDomainModel()
        {
            // ===================================
            //            PREPARATION
            // ===================================
            var provider = new SqlProvider(connstring);
            var taskRepository = new TaskRepository(provider);

            // manager
            var taskManager = new TaskManager(taskRepository);
            var task = new UserTask
            {
                UserId = 2,
                TaskName = "Go to supermarket to buy some milk"
            };

            // insert task to DB
            task.TaskId = taskManager.InsertTask(task);
            Console.WriteLine($"Inserted TaskId: {task.TaskId}");

            // update task in DB
            task.TaskName = "Do the home work";
            taskManager.UpdateTask(task);
            Console.WriteLine("Updated task successfully");

            // delete task from DB
            taskManager.DeleteTask(task);
            Console.WriteLine("Deleted task successfully");
        }
    }
}

﻿using Lesson17.Interfaces;
using Lesson17.Models;
using Lesson17.Abstract;
using Lesson17.Interfaces.Repository;

namespace Lesson17.Repositories
{
    public class TaskRepository : AbstractRepository, ITaskRepository
    {
        public TaskRepository(IProvider provider): base("Tasks", "TaskId", provider)
        {
        }

        public UserTask Get(int id)
        {
            var result = new UserTask();
            string query = $"SELECT * FROM dbo.{TableName} \r\n" +
                           $"WHERE {PrimaryKeyName} = {id}";

            var values = Provider.Get(query);

            // init result
            if (values != null && values.Length > 0)
            {
                result = new UserTask()
                {
                    TaskId = (values[0] as int?) ?? 0,
                    TaskName = values[1]?.ToString()
                };
            }

            return result;
        }

        public int InsertRecord(UserTask record)
        {
            // Use reflection later
            var result = 0;

            string query = $"INSERT INTO dbo.{TableName} (TaskName, UserId) \r\n" +
                           $"VALUES (N'{record.TaskName}', {record.UserId}) \r\n" +
                           "SELECT CAST(scope_identity() AS int)";
            string temp = this.Provider.ExecuteScalar(query);
            int.TryParse(temp, out result);

            return result;
        }

        public void UpdateRecord(UserTask record)
        {
            // Use reflection later
            string query = $"UPDATE dbo.{TableName} \r\n" +
                           $"SET TaskName = N'{record.TaskName}' \r\n" +
                           $"WHERE {PrimaryKeyName} = {record.TaskId}";
            this.Provider.ExecuteQuery(query);
        }

        public void DeleteRecord(int id)
        {
            string query = $"DELETE FROM dbo.{TableName} \r\n" +
                           $"WHERE {PrimaryKeyName} = {id}";
            this.Provider.ExecuteQuery(query);
        }
    }
}

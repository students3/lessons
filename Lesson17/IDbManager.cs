﻿using System.Data.SqlClient;

namespace Lesson17
{
    public interface IDbManager
    {
        void OpenConnection();
        void CloseConnection();
        SqlDataReader GetReader(string query);
        int ExecuteNonQuery(string query);
    }
}

﻿using Lesson17.Models;

namespace Lesson17.Interfaces.Manager
{
    public interface ITaskManager
    {
        UserTask GetTask(int id);
        int InsertTask(UserTask record);
        void UpdateTask(UserTask record);
        void DeleteTask(UserTask record);
    }
}

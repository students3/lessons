﻿using Lesson17.Models;

namespace Lesson17.Interfaces.Repository
{
    public interface ITaskRepository: IRepositoryBase<UserTask>
    {
    }
}

﻿namespace Lesson17.Interfaces.Repository
{
    public interface IRepositoryBase<T> where T : IEntity
    {
        // CRUD
        T Get(int id);
        int InsertRecord(T record);
        void UpdateRecord(T record);
        void DeleteRecord(int id);
    }
}

﻿namespace Lesson17.Interfaces
{
    public interface IProvider
    {
        void Open();
        void Close();
        object[] Get(string query);
        void ExecuteQuery(string query);
        string ExecuteScalar(string query);
    }
}

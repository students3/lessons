﻿using Lesson17.Interfaces;

namespace Lesson17.Models
{
    public class UserTask: IEntity
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public int UserId { get; set; }
    }
}

﻿using Lesson20.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Lesson20
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestLinq1();
            //TestOrderBy();
            //TestGroupBy();
            TestJoin();

            Console.ReadLine();
        }

        private static void TestLinq1()
        {
            // 1. Data source
            int[] numbers = new int[5] { 0, 1, 2, 3, 4 };

            // 2.1) Query creation
            var numQuery = from num in numbers  // when joining some collection OR groupping collection
                           where (num % 2) == 0
                           select num;

            // 3.2) Query execution.
            foreach (int num in numQuery)
            {
                Console.Write("{0, 1} ", num);
            }

            Console.WriteLine();

            // 2.2) Query creation
            var numQuery2 = numbers.Where(x => x % 2 == 0).ToArray();

            // 3.2) Query execution.
            foreach (int num in numQuery2)
            {
                Console.Write("{0, 1} ", num);
            }
        }

        private static void TestOrderBy()
        {
            var persons = new List<Person>(new[] {
               new Person { FirstName="Svetlana", LastName="Odarko", ID=1111, Age = 18 },
               new Person { FirstName="Petro", LastName="Petrenko", ID=1112, Age = 25 },
               new Person { FirstName="Gerasym", LastName="Gerasymenko", ID=1113, Age = 30 },
               new Person { FirstName="Vasyl", LastName="Ivanenko", ID=1114, Age = 25 },
               new Person { FirstName="Zakhar", LastName="Sergienko", ID=1115, Age = 20 }
           });

            var query = (from itm in persons
                         orderby itm.FirstName descending, itm.Age ascending
                         select itm).ToArray();

            foreach (var item in query)
            {
                Console.WriteLine($"{item.FirstName} {item.LastName} {item.Age}");
            }

            Console.WriteLine();

            var query2 = persons.OrderByDescending(x => x.FirstName)
                                .ThenBy(x => x.Age)
                                .ToArray();

            //var dtInput = new DataTable();
            //var rows = dtInput.AsEnumerable().Where(x => x.RowState == DataRowState.Added)
            //                                 .ToArray();

            foreach (var item in query2)
            {
                Console.WriteLine($"{item.FirstName} {item.LastName} {item.Age}");
            }
        }

        private static void TestGroupBy()
        {
            char letter = "Fomenko"[0]; //.ToCharArray()[0]

            var persons = new List<Person>(new[] {
               new Person { FirstName="Vasyl", LastName="Skakun", ID=1114, Age = 25 },
               new Person { FirstName="Zakhar", LastName="Sergienko", ID=1115, Age = 20 },
               new Person { FirstName="Svetlana", LastName="Fomenko", ID=1111, Age = 18 },
               new Person { FirstName="Petro", LastName="Furman", ID=1112, Age = 25 },
               new Person { FirstName="Gerasym", LastName="Gerasymenko", ID=1113, Age = 30 }
           });

            var sortedGroups = from itm in persons
                                   //orderby itm.LastName, itm.FirstName
                               group itm by itm.LastName[0] into group1
                               orderby group1.Key
                               select new { group1.Key, Persons = group1 };

            foreach (var item in sortedGroups)
            {
                //var names = string.Join(" / ", item.Persons.Select(x => x.LastName).ToArray());
                var names = string.Join(" / ", item.Persons.Select(x => x.ToString()).ToArray());

                Console.WriteLine($"Key: {item.Key}, persons: {names}");
            }
        }

        private static void TestJoin()
        {
            var owner1 = new Owner()
            {
                OwnerId = 1,
                FirstName = "Viktor",
                LastName = "Zavrazhnov"
            };

            var animal1 = new Animal()
            {
                AnimalId = 89,
                NickName = "Tuzik",
                Type = "Dog",
                OwnerId = 1
            };

            var animal2 = new Animal()
            {
                AnimalId = 95,
                NickName = "Murchik",
                Type = "Cat",
                OwnerId = 1
            };

            var owners = new List<Owner>();
            owners.Add(owner1);

            var animals = new List<Animal>();
            animals.AddRange(new Animal[] { animal1, animal2 });

            // INNER JOIN (SQL)
            var query = from itm1 in owners
                        join itm2 in animals on itm1.OwnerId equals itm2.OwnerId
                        select new { itm1.OwnerId, itm1.FirstName, itm1.LastName, itm2.NickName };

            // GROUP BY (SQL)
            var query2 = from itm1 in owners
                         join itm2 in animals on itm1.OwnerId equals itm2.OwnerId
                         group itm2 by itm2.OwnerId into g1
                         select new { OwnerId = g1.Key, Animals = g1.ToArray() };

            // LEFT JOIN (SQL)

            //var leftOuterQuery = from team in teams
            //                     join p in persons on team.ID equals p.FootballTeamID into pGroup
            //                     select pGroup.DefaultIfEmpty(new Person()
            //                     {
            //                         FirstName = "NoName",
            //                         LastName = "NoLastName",
            //                         FootballTeamID = team.ID
            //                     });

            foreach (var item in query)
            {
                Console.WriteLine($"FirstName: {item.FirstName}, LastName: {item.FirstName} owns {item.NickName}");
            }

        }
    }
}

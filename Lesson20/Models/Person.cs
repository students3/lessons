﻿namespace Lesson20.Models
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return $"{this.LastName} {this.FirstName}";
        }
    }
}

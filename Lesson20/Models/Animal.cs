﻿namespace Lesson20.Models
{
    class Animal
    {
        public int AnimalId { get; set; }
        public string NickName { get; set; }
        public string Type { get; set; }

        public int OwnerId { get; set; }
    }
}

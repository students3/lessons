﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson3
{
    public static class GlobalConstants
    {
        public static double Pi { get; private set; }
        public static string AppName { get; private set; }

        static GlobalConstants()
        {
            Pi = 3.14;
            AppName = "My application";
        }
    }
}

﻿using Lesson3.Model;
using Lesson3.Model.Excel;
using System;
using System.Drawing;

namespace Lesson3
{
    class Program
    {
        static void Main(string[] args)
        {
            //TestConstants();
            //TestOrder();
            TestExcel();

            int[] arrTemp = new int[] { 556, 54 };
            var t = arrTemp[0];

            //List<int> list = new List<int>();
            //Tuple<int, int, int> cortage = new Tuple<int, int, int>(1, 2, 1);
            //cortage.Item1

            Console.ReadLine();
        }

        private static void TestConstants()
        {
            Console.WriteLine(GlobalConstants.Pi);
            Console.WriteLine(GlobalConstants.AppName);

            //GlobalConstants.Pi = 5.25;
        }

        private static void TestAccesModifiers()
        {
            Product product = new Product();
            //product.GetPrice
        }

        private static void TestExcel()
        {
            WorkBook wb = new WorkBook(); //WorkBook[0]
            Cell cell = wb.Sheets[0].Rows[0].Cells[1];
            cell.Value = "Hello vasya";

            WorkSheet sh = new WorkSheet();
            WorkSheet sh2 = new WorkSheet("new-sheet");

            Cell.MyClass df = new Cell.MyClass();

            Console.WriteLine("Cell value: " + cell.Value);
            Console.WriteLine("Cell forecolor: " + cell.ForeColor.ToString());
            Console.WriteLine("Cell background: " + cell.BackgroundColor.ToString());
        }

        private static void TestOrder()
        {
            //Order order = new Order();
            //double total = order.GetTotalPrice(5);
            var shoppingCart = new ShoppingCart()
            {
                Order = new Order()
                {
                    OrderNumber = Guid.NewGuid().ToString().Replace("-", "")
                }
            };

            shoppingCart.Order.AddProduct(new Product()
            {
                ProductName = "Laptop",
                DateCreated = DateTime.Now,
                Manufacturer = "Asus",
                Price = 4000
            }, 5);

            //ProductInOrder productInOrder = new ProductInOrder();
            //productInOrder.Cart = new ShoppingCart();
            //var cart = productInOrder.Cart;
        }

        private static void TestBitmap()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string imgPath = System.IO.Path.Combine(path, "21.jpg");
            string flag = System.IO.File.Exists(imgPath) ? "exists" : "not found";

            var bmp = Bitmap.FromFile(imgPath);
            bmp.Dispose();

            // Display on the 3 monitors the same reference bmp

            Console.WriteLine(flag);
        }
    }
}

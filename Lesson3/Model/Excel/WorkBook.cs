﻿namespace Lesson3.Model.Excel
{
    public class WorkBook
    {
        public WorkSheet[] Sheets { get; private set; }

        public WorkBook()
        {
            Sheets = new WorkSheet[] { new WorkSheet() };
        }
    }
}

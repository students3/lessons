﻿namespace Lesson3.Model.Excel
{
    public class Row
    {
        public Cell[] Cells { get; set; }

        public Row()
        {
            this.Cells = new Cell[] { new Cell(), new Cell() };
        }
    }
}

﻿namespace Lesson3.Model.Excel
{
    public enum CellColor
    {
        White,
        Red,
        Green,
        Blue,
        Black
    }
}

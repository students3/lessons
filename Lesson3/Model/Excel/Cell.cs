﻿namespace Lesson3.Model.Excel
{
    public class Cell
    {
        public string Value { get; set; }
        public CellColor ForeColor { get; set; }
        public CellColor BackgroundColor { get; set; }

        public Cell()
        {
            this.Value = string.Empty;
            this.ForeColor = CellColor.Black;
        }

        public class MyClass
        {
            public int MyProperty { get; set; }
        }
    }
}

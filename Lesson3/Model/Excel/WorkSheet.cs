﻿namespace Lesson3.Model.Excel
{
    public class WorkSheet
    {
        public Row[] Rows { get; set; }
        public string SheetName { get; set; }

        public WorkSheet(string sheetName)
        {
            SheetName = sheetName;
        }

        public WorkSheet()
        {
            Rows = new Row[] { new Row() };
        }
    }
}

﻿namespace Lesson3.Model
{
    public class ProductInOrder : Product
    {
        public ProductInOrder(Product product)
        {
            this.ProductName = product.ProductName;
            this.Price = product.Price;
            this.Manufacturer = product.Manufacturer;
            this.DateCreated = product.DateCreated;

            var price = base.GetPrice();
        }

        private int _qty;
        public int Qty
        {
            get { return _qty; }
            set { _qty = value * 2; }
        }

        //public int MyQty { get; private set; }

        public override string GetManufactureName()
        {
            return "Vasya";
        }

        public new string GetName()
        {
            return this.ProductName + " was bought in qty " + this.Qty;
        }
    }
}

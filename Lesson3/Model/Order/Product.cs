﻿using System;

namespace Lesson3.Model
{
    public class Product
    {
        public string ProductName { get; set; }

        public string Manufacturer { get; set; }

        public DateTime DateCreated { get; set; }

        public double Price { get; set; }

        public virtual string GetManufactureName()
        {
            return this.Manufacturer;
        }

        public string GetName()
        {
            return this.ProductName;
        }

        internal double GetPrice()
        {
            return this.Price;
        }
    }
}

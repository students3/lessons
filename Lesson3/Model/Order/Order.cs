﻿using Lesson3.Model;
using System.Collections.Generic;

namespace Lesson3
{
    public class Order
    {
        private List<ProductInOrder> _items;

        public Order()
        {
            _items = new List<ProductInOrder>();
        }

        public string OrderNumber { get; set; }

        public void AddProduct(Product product, int qty)
        {
            // copy other varables from => product
            ProductInOrder prodInOrder = new ProductInOrder(product)
            { 
                Qty = qty
            };

            _items.Add(prodInOrder);
        }

        public double GetTotalPrice(int percents)
        {
            double result = 0;

            foreach (var item in _items)
            {
                result += item.Price * item.Qty;
            }

            if (percents > 0)
            {
                result = this.ApplyDiscount(result, percents);
            }

            return result;
        }

        private double ApplyDiscount(double orderTotal, int percents)
        {
            double result = orderTotal;

            if (percents > 0)
            {
                // apply discount
                result = orderTotal - orderTotal * percents / 100;
            }

            return result;
        }
    }
}

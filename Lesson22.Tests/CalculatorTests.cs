﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lesson22.Tests
{
    [TestClass]
    public class CalculatorTests
    {
        [TestMethod]
        public void Multiply_2by6_Expected12()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var computed = calculator.Multiply(2, 6);

            // Assert
            Assert.AreEqual(12, computed);
        }

        [TestMethod]
        public void Multiply_2by7_Expected14()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var computed = calculator.Multiply(2, 7);

            // Assert
            Assert.AreEqual(14, computed);
        }

        [TestMethod]
        public void Sum_2and7_Expected9()
        {
            // Arrange
            var calculator = new Calculator();

            // Act
            var computed = calculator.Sum(2, 7);

            // Assert
            Assert.AreEqual(9, computed);
        }

        private string SayHello()
        {
            return "Say Hello";
        }

        // Assert
        // CollectionAssert
        // StringAssert
        // AssertFailedException
        // AssertInconclusiveException
    }
}

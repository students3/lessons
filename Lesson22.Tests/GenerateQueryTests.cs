﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lesson22.Tests
{
    [TestClass]
    public class GenerateQueryTests
    {
        [TestMethod]
        public void Test_Generator_Success()
        {
            // Arrange
            var builder = new QueryBuilder();

            // Act
            var computed = builder.GenerateQuery("products");

            // Assert
            Assert.AreEqual("SELECT * FROM dbo.products", computed);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Table is not handled !")]
        public void Test_Generator_Failure()
        {
            // Arrange
            var builder = new QueryBuilder();

            // Act
            builder.GenerateQuery("jdhdfhgkhdfg");

            // Assert
            Assert.Fail();
        }

        [TestMethod]
        public void Test_Generator_DF()
        {
            // Arrange
            var builder = new QueryBuilder();

            // Act
            builder.GenerateQuery("jdhdfhgkhdfg");

            // Assert
            Assert.Fail();
        }
    }
}

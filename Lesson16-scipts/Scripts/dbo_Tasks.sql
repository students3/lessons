﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.5.196.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 11/19/2017 10:34:55 AM
-- Версия сервера: 11.00.6251
--

SET DATEFORMAT ymd
SET ARITHABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS, XACT_ABORT OFF
GO

SET IDENTITY_INSERT Lesson16.dbo.Tasks ON
GO
INSERT Lesson16.dbo.Tasks(TaskId, TaskName, UserId, Completed, EnviromentId, FinishedDate) VALUES (1, N'update price on the site', 1, CONVERT(bit, 'False'), 1, NULL)
INSERT Lesson16.dbo.Tasks(TaskId, TaskName, UserId, Completed, EnviromentId, FinishedDate) VALUES (2, N'import products to site', 2, CONVERT(bit, 'True'), 1, NULL)
INSERT Lesson16.dbo.Tasks(TaskId, TaskName, UserId, Completed, EnviromentId, FinishedDate) VALUES (3, N'add images', 1, CONVERT(bit, 'True'), 2, NULL)
INSERT Lesson16.dbo.Tasks(TaskId, TaskName, UserId, Completed, EnviromentId, FinishedDate) VALUES (4, N'delete Audi', 1, CONVERT(bit, 'False'), 2, NULL)
GO
SET IDENTITY_INSERT Lesson16.dbo.Tasks OFF
GO
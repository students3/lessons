﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.5.196.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 11/19/2017 10:34:55 AM
-- Версия сервера: 11.00.6251
--

SET DATEFORMAT ymd
SET ARITHABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS, XACT_ABORT OFF
GO

SET IDENTITY_INSERT Lesson16.dbo.Roles ON
GO
INSERT Lesson16.dbo.Roles(RoleId, RoleName) VALUES (1, N'Admin')
INSERT Lesson16.dbo.Roles(RoleId, RoleName) VALUES (2, N'User')
GO
SET IDENTITY_INSERT Lesson16.dbo.Roles OFF
GO
﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.5.196.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 11/19/2017 10:34:55 AM
-- Версия сервера: 11.00.6251
--

SET DATEFORMAT ymd
SET ARITHABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS, XACT_ABORT OFF
GO

SET IDENTITY_INSERT Lesson16.dbo.UserAdditionalInfo ON
GO
INSERT Lesson16.dbo.UserAdditionalInfo(AdditionalInfoId, Email, Phone, INN, UserId) VALUES (2, N'andrew@gmail.com', N'565-98-23', 295340078655, 1)
INSERT Lesson16.dbo.UserAdditionalInfo(AdditionalInfoId, Email, Phone, INN, UserId) VALUES (3, N'oleg@gmail.com', N'655-98-89', 656564854144, 2)
INSERT Lesson16.dbo.UserAdditionalInfo(AdditionalInfoId, Email, Phone, INN, UserId) VALUES (4, N'alexandr@gmail.com', N'6565-98989-65', 65565656, 3)
INSERT Lesson16.dbo.UserAdditionalInfo(AdditionalInfoId, Email, Phone, INN, UserId) VALUES (5, N'alexandra@gmail.com', N'8888-98989-65', 111111, 4)
GO
SET IDENTITY_INSERT Lesson16.dbo.UserAdditionalInfo OFF
GO
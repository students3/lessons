﻿--
-- Скрипт сгенерирован Devart dbForge Studio for SQL Server, Версия 5.5.196.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/sql/studio
-- Дата скрипта: 11/19/2017 10:34:55 AM
-- Версия сервера: 11.00.6251
--

SET DATEFORMAT ymd
SET ARITHABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
SET NUMERIC_ROUNDABORT, IMPLICIT_TRANSACTIONS, XACT_ABORT OFF
GO

SET IDENTITY_INSERT Lesson16.dbo.UserRoles ON
GO
INSERT Lesson16.dbo.UserRoles(UserRoleId, UserId, RoleId) VALUES (1, 1, 1)
INSERT Lesson16.dbo.UserRoles(UserRoleId, UserId, RoleId) VALUES (3, 1, 2)
INSERT Lesson16.dbo.UserRoles(UserRoleId, UserId, RoleId) VALUES (4, 2, 1)
GO
SET IDENTITY_INSERT Lesson16.dbo.UserRoles OFF
GO
﻿--SELECT
--  COUNT(*)
--FROM dbo.Tasks t;

SELECT
  t.UserId,
 COUNT(t.TaskId) AS [TasksQty]
FROM dbo.Tasks t
GROUP BY t.UserId
--HAVING COUNT(t.TaskId) > 1;


﻿SELECT
  *
FROM dbo.Tasks t
WHERE t.UserId IN (
  SELECT
    UserId
  FROM dbo.Users
  WHERE UserName = 'Andrew');
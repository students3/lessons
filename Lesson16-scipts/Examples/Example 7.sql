﻿SELECT
  *
FROM dbo.Tasks t
WHERE t.UserId = 1
UNION
SELECT
  *
FROM dbo.Tasks t
WHERE t.UserId = 3;


SELECT
  *
FROM dbo.Tasks t
--WHERE t.UserId = 1 OR t.UserId = 3
WHERE t.UserId IN (1, 3)
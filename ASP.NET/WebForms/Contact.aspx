﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebForms.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Your contact page.</h3>
    <div>
        One Microsoft Way<br />
        Redmond, WA 98052-6399<br />
        <abbr title="Phone">P:</abbr>
        425.555.0100
    </div>

    <div style="margin-bottom: 40px;">
        <strong>Support:</strong><a href="mailto:Support@example.com">Support@example.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
    </div>

    <asp:Calendar ID="Calendar1" runat="server" FirstDayOfWeek="Monday" OnSelectionChanged="Unnamed1_SelectionChanged"></asp:Calendar>
    <br />
    <asp:Label ID="Label1" runat="server" Text="Label" OnClick="colorCalendar()"></asp:Label>

    <script type="text/javascript">
        function colorCalendar() {
            var calendarId = "<%=Calendar1.ClientID%>";
            jQuery("#" + calendarId).css({ "background-color": "greenyellow" });
            //console.log("Hello world from Contact page. \r\nCalendar Id is: " + calendarId);
        }
    </script>
</asp:Content>

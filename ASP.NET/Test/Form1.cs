﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int _x;
        private int _y;

        private void Form1_Load(object sender, EventArgs e)
        {
            _x = this.ClientRectangle.Width / 2;
            _y = this.ClientRectangle.Height / 2;
        }

        private void DrawCirle(int x, int y, int radius)
        {
            var myBrush = new SolidBrush(Color.Red);
            var formGraphics = this.CreateGraphics();

            formGraphics.FillEllipse(myBrush, new Rectangle(x - radius / 2, y - radius / 2, radius, radius));
            myBrush.Dispose();
            formGraphics.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DrawCirle(this.ClientRectangle.Width / 2, this.ClientRectangle.Height / 2, 100);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _x += 20;
            _y -= 20;

            this.DrawCirle(_x, _y, 100);
        }
    }
}

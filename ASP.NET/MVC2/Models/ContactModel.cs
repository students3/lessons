﻿namespace MVC2.Models
{
    public class ContactModel
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string Likes { get; set; }
    }
}
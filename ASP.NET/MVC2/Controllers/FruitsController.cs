﻿using System.Web.Mvc;

namespace MVC2.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class FruitsController : Controller
    {
        // GET: Fruits
        public ActionResult Index()
        {
            return View();
        }
    }
}
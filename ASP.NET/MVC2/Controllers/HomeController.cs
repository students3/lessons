﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MVC2.Models;

namespace MVC2.Controllers
{
    public class HomeController : Controller
    {
        private readonly List<ContactModel> _contacts;

        public HomeController()
        {
            _contacts = new List<ContactModel>
            {
                new ContactModel
                {
                    ContactId = 1,
                    Name = "Alexandra",
                    Likes = "Icecream, chocolate"
                },
                new ContactModel
                {
                    ContactId = 2,
                    Name = "Oleg",
                    Likes = "Girls, whiskey"
                }
            };
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact(string id)
        {
            var contactId = 0;
            int.TryParse(id, out contactId);
            var model = _contacts.Any(x => x.ContactId == contactId)
                                       ? _contacts.First(x => x.ContactId == contactId)
                                       : new ContactModel();

            //var message = !string.IsNullOrEmpty(id)
            //       ? id.ToUpper() + ", this is your contact page."
            //       : "Your contact page.";
            //ViewBag.Message = message;

            ViewBag.Message = id;

            return View(model);
        }
    }
}
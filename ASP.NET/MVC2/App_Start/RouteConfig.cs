﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MVC2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Contacts",
                url: "Find/MyContacts/{id}",
                defaults: new { controller = "Home", action = "Contact", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{*catchall}",
                defaults: new { controller = "Home", action = "Index" },
                constraints: new
                {
                    controller = "(Account|Home|Fruits|Manage)", id = @"\d{2}"
                });

            // display 404 error
            // Houston we have a problem
            routes.MapRoute(
                name: "404",
                url: "{*catchall}",
                defaults: new { controller = "Error", action = "Error404" });


            //routes.MapPageRoute("Mylogout", "Contacts/Default", "~/default.aspx", false);
        }
    }
}

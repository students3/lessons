﻿namespace WebApi.Models.Users
{
    public class UserInfo
    {
        public int UserId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public int Age { get; set; }
    }
}
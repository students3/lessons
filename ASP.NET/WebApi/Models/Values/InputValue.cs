﻿namespace WebApi.Models.Values
{
    public class InputValue
    {
        public string Value { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Web.Http;
using WebApi.Models.Values;

namespace WebApi.Controllers
{
    //[Authorize]
    [RoutePrefix("values")]
    public class ValuesController : ApiController
    {
        private static List<string> _values = new List<string>(new[] { "apples", "tomatoes" });

        // GET api/values
        public IEnumerable<string> GetAll()
        {
            return _values;
        }

        // GET api/values/5
        public IHttpActionResult Get([FromUri]int id)
        {
            IHttpActionResult result;

            if (id < 1)
            {
                result = BadRequest("Can't find element with Id: " + id);
            }
            else if (id >= 1 && _values.Count >= id)
            {
                var value = _values[id - 1];
                result = Ok(value);
            }
            else
            {
                result = NotFound();
            }

            // common function

            return result;
        }

        // POST api/values
        public void Post([FromBody] InputValue input)
        {
            if (!string.IsNullOrEmpty(input?.Value)) _values.Add(input.Value);
        }

        // PUT api/values/5
        public string Put(int id, [FromBody]InputValue input)
        {
            var result = ValidateInput(id, input);

            if (string.IsNullOrEmpty(result))
            {
                _values[id - 1] = input.Value;
                result = "Success";
            }

            return result;
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            if (id >= 1 && _values.Count >= id)
            {
                _values.RemoveAt(id - 1);
            }
        }

        private string ValidateInput(int id, InputValue input)
        {
            var result = string.Empty;

            if (input == null)
            {
                result = "Input model is null";
            }
            else if (string.IsNullOrEmpty(input.Value))
            {
                result = "Please pass non empty Value";
            }
            else if (id < 1)
            {
                result = "Please pass Id more than 0";
            }
            else if (_values.Count < id)
            {
                result = "Can't find value with Id: " + id;
            }

            return result;
        }
    }
}

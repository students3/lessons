﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebApi.Models.Users;

namespace WebApi.Controllers
{
    public class UsersController : ApiController
    {
        UserInfo[] users =  {
            new UserInfo
            {
                UserId = 1,
                FirstName = "Vasia",
                LastName = "Vasechkin",
                Age = 20
            },
            new UserInfo {
                UserId = 2,
                FirstName = "Ivan",
                LastName = "Ivanov",
                Age = 22
            }
        };

        public IEnumerable<UserInfo> GetAllUsers()
        {
            return users;
        }

        public IHttpActionResult GetUser(int id)
        {
            var user = users.FirstOrDefault(u => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
    }
}
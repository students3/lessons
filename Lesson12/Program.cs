﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;

namespace Lesson12
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string txtFile = Path.Combine(path, "Smart SQL filter.txt");
            string zipFile = Path.Combine(path, txtFile + ".zip");

            string output = Path.Combine(path, "Fruits.txt");

            //TestGzip(txtFile, zipFile);

            //TestWriteFile(output);
            //TestWriteFile(output, true);

            TestReadFile(output);

            Console.WriteLine("Completed");
            Console.ReadLine();
        }

        private static void TestStreams()
        {
            var stream = System.IO.File.OpenRead("D:\\myfile.txt");       // error - OutOfMemory (buffer)

            var lines = System.IO.File.ReadLines("D:\\myfile.txt");       // 800Mb - ok
            var result2 = System.IO.File.ReadAllLines("D:\\myfile.txt");  // error - OutOfMemory (all was loaded to memory)

            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }

            // ADO.NET
            // DataTable  <= Server loads all data at once
            // DataReader <= Server loads only one row at once
        }

        private static void TestGzip(string inputFile, string outFile)
        {
            FileStream sorce = File.OpenRead(inputFile);
            FileStream destination = File.Create(outFile);
            GZipStream zipStream = new GZipStream(destination, CompressionMode.Compress);

            int oneByte = sorce.ReadByte();

            while (oneByte != -1)
            {
                zipStream.WriteByte((byte)oneByte);
                oneByte = sorce.ReadByte();
            }

            sorce.Close();
            zipStream.Close();
            destination.Close();
        }

        private static void TestGzip2(string inputFile, string outFile)
        {
            using (FileStream sorce = File.OpenRead(inputFile))
            {
                using (FileStream destination = File.Create(outFile))
                {
                    GZipStream zipStream = new GZipStream(destination, CompressionMode.Compress);

                    int oneByte = sorce.ReadByte();

                    while (oneByte != -1)
                    {
                        zipStream.WriteByte((byte)oneByte);
                        oneByte = sorce.ReadByte();
                    }

                    sorce.Close();
                    zipStream.Close();
                    destination.Close();
                }
            }

            using (var myBitmap = new Bitmap("D:\\apple.jpg"))
            {

            }

            var myBitmap2 = new Bitmap("D:\\apple.jpg");
            myBitmap2.Dispose();

            DataTable dtInput = new DataTable();
            dtInput.Rows.Clear();
            dtInput.AcceptChanges();
            dtInput.Dispose();
        }

        private static void TestWriteFile(string inputFile, bool append = false)
        {
            using (var writer = new StreamWriter(inputFile, append))
            {
                string[] fruits = new string[] { "яблоко", "груша", "pamela", "grape" };

                for (int i = 0; i < fruits.Length; i++)
                {
                    writer.WriteLine(DateTime.Now.ToString("g") + " " + fruits[i]);
                }

                writer.Close();
            }
        }

        private static void TestReadFile(string inputFile)
        {
            //var lines = File.ReadLines(inputFile);

            //foreach (var line in lines)
            //{
            //    Console.WriteLine(line);
            //}

            using (var sr = new StreamReader(inputFile))
            {
                while (sr.Peek() >= 0)
                {
                    Console.WriteLine(sr.ReadLine());
                }

                sr.Close();
            }
        }

        //private static void TestGzipEnhanced(string inputFile)
        //{
        //    FileInfo fsSource = new FileInfo(inputFile);
        //    FileStream sorce = fsSource.OpenRead();
        //    FileStream compressedFileStream = File.Create(fsSource.FullName + ".zip");
        //    GZipStream zipStream = new GZipStream(compressedFileStream, CompressionMode.Compress);

        //    sorce.Close();
        //    zipStream.Close();
        //    //compressedFileStream.Close();
        //}
    }
}

﻿using EF.CodeFirst.Models;
using System.Data.Entity;

namespace EF.CodeFirst.Context
{
    public class UnivercityContext: DbContext
    {
        public UnivercityContext(): base("UnivercityEntities")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Email> Emails { get; set; }           // email table
        public DbSet<Lecturer> Lecturers { get; set; }     // lecturer table
    }
}

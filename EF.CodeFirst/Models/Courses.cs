﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF.CodeFirst.Models
{
    [Table("Courses")]
    public class Courses
    {
        [Key]
        [Column(Order = 1)]
        public int CourseId { get; set; }

        [Key]
        [Column(Order = 2)]
        public int LecturerId { get; set; }

        public string CourseName { get; set; }


        public virtual Lecturer Lecturer { get; set; }              // navigation property
    }
}

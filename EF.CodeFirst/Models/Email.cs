﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF.CodeFirst.Models
{
    [Table("Emails")]
    public class Email
    {
        [Key]
        public int EmailId { get; set; }
        public string EmailValue { get; set; }
        public int LecturerId { get; set; }

        // even email have 1 field for foreign key from lecturer,
        //virtual allow for it to be overridden in a derived class
        public virtual Lecturer Lecturer { get; set; }              // navigation property
    }
}

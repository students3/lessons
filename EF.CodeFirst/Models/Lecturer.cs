﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF.CodeFirst.Models
{
    [Table("Lecturers")]
    public class Lecturer
    {
        [Key]
        public int LecturerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int Age { get; set; }

        // field foreign key in email: 1 lecturer may have N emails
        public virtual ICollection<Email> Emails { get; set; }
    }
}

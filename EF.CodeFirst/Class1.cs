﻿using EF.CodeFirst.Context;

namespace EF.CodeFirst
{
    class Class1
    {
        private void Test()
        {
            using (var context = new UnivercityContext())
            {
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
            }
        }
    }
}


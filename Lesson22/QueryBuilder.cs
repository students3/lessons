﻿using System;

namespace Lesson22
{
    public class QueryBuilder
    {
        public string GenerateQuery(string tableName)
        {
            var result = string.Empty;

            switch (tableName)
            {
                case "products":
                    result = $"SELECT * FROM dbo.{tableName}";
                    break;

                case "categories":
                    result = $"SELECT * FROM dbo.{tableName}";
                    break;

                default:
                    throw new ArgumentException($"Table {tableName} is not handled !");
            }

            return result;
        }
    }
}

﻿namespace Lesson22
{
    public class Calculator
    {
        public int Multiply(int x, int y)
        {
            return x * y;
        }

        public int Sum(int x, int y)
        {
            return x + y;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lessons
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 0; i < 10; i++)
            //{
            //    int j = i & 0xFFFE;
            //    Console.WriteLine(j);
            //}



            //if (j > 4)
            //{
            //    Console.WriteLine("Variable more than 4");
            //}
            //else
            //{
            //    Console.WriteLine("Variable less than 4");
            //}

            // jump statements:  break, return, continue,        goto
            //int j = 3;
            //string result = GetNumber(j);

            //TestContinue();

            //string[] fruits = { "grape", "banana", "apple", "pear" };
            //string[] fruits2 = new string[4];
            //fruits2[0] = "grape";
            //fruits2[1] = "banana";

            //Array.Sort(fruits);
            //fruits = fruits.OrderBy(x => x).ToArray();

            //foreach (var item in fruits)
            //{
            //    Console.WriteLine(item);
            //}

            //TestFor(fruits);

            //string result = "    ";
            //int i = result.Length;

            //Console.WriteLine(result);
            //Console.ReadLine();

            //SwitchStatementTest();

            //DoLoop();

            //WhileStatement();

            //ForEachTest(fruits);

            //TestJaggedArray();

            //int j = ArraySearch(fruits, "apple");

            int j = (int)Marks.Excellent;

            Console.WriteLine(j);
            Console.WriteLine(Marks.Excellent.ToString());
            Console.ReadLine();
        }

        private static void LinqTest(string[] fruits)
        {
            var filter = fruits.Where(x => x.Length <= 5)
                              .OrderBy(x => x)
                              .ToArray();

            //var query = (from itm in fruits
            //             where itm.Length <= 5
            //             select new { x });

            foreach (var item in filter)
            {
                Console.WriteLine(item);
            }
        }

        private static int ArraySearch(string[] fruits, string target)
        {
            int result = -1;

            //return Array.BinarySearch(fruits, target);
            for (int i = 0, length = fruits.Length; i < length; i++)
            {
                if (string.Equals(fruits[i], target, StringComparison.InvariantCultureIgnoreCase))
                {
                    result = i;
                    break;
                }
            }

            return result;
        }

        private static void TestJaggedArray()
        {
            int[][] arrTemp = new int[2][];
            arrTemp[0] = new int[3];
            arrTemp[1] = new int[1];
            int rank = arrTemp.Rank;

            int[,] arrTemp2 = new int[2, 3];
            arrTemp2[0, 2] = 5;
            int rank2 = arrTemp2.Rank;
            int lenBound1 = arrTemp2.GetLength(1);

            // Rows
            for (int rowId = 0, rows = arrTemp2.GetLength(0); rowId < rows; rowId++)
            {
                //Columns
                for (int colId = 0, columns = arrTemp2.GetLength(1); colId < columns; colId++)
                {
                    Console.WriteLine("Row " + (rowId + 1) +
                                      ", column " + (colId + 1) +
                                      " has value " + arrTemp2[rowId, colId]);
                }

                Console.WriteLine(new string('-', 30));
            }

            // int   =>  projectId
            // int[] =>  userIds
            Dictionary<int, string[]> dicTemp = new Dictionary<int, string[]>
            {
                { 1, new string[] { "15", "12", "25" } },
                { 2, new string[] { "89" } }
            };
        }

        private static void ForEachTest(string[] fruits)
        {
            foreach (var f in fruits)
            {
                Console.WriteLine(f);
            }
        }

        private static void ForEachTest(int[] numbers)
        {
            var j = 0;
            var carName = new System.Data.DataTable();

            Console.WriteLine(j);
            Console.WriteLine(carName);

            //var t = numbers.GetType();
            foreach (int f in numbers)
            {
                Console.WriteLine(f);
            }
        }

        private static void DoLoop()
        {
            string read = string.Empty;
            Console.WriteLine("Press key q to exit");

            do
            {
                Console.WriteLine("Your choice ...");
                read = Console.ReadLine();

                Console.WriteLine("You wrote: " + read);
                Console.WriteLine(new string('-', 20));
            } while (read != "q");
        }

        private static void WhileStatement()
        {
            int j = 0;
            DateTime dt = DateTime.Now.AddSeconds(1);

            while (dt > DateTime.Now)
            {
                j++;
                if (j < 1000) continue;
                Console.WriteLine(j);
            }
        }

        // {"grape", "banana", "apple", "pear"}
        private static void TestFor(string[] fruits)
        {
            for (int i = 0, limit = fruits.Length; i < limit; i++)
            {
                if (string.Equals(fruits[i], "apple", StringComparison.InvariantCultureIgnoreCase))
                {
                    break;
                }
                else
                {
                    Console.WriteLine(fruits[i]);
                }
            }
        }

        private static void TestContinue()
        {
            for (int i = 0; i <= 15; i++)
            {
                if (i < 5) continue;
                Console.WriteLine(i);

                //if (i >= 5)
                //{
                //    Console.WriteLine(i);
                //}
            }
        }

        private static string GetNumber(int input)
        {
            string result = string.Empty;

            if (input == 1)
            {
                return "One";
            }
            else if (input == 2)
            {
                return "Two";
            }
            else if (input == 3)
            {
                result = "";
            }

            if (string.IsNullOrEmpty(result))
            {
                //log error
            }

            return result;
        }

        private static string SwitchStatementTest()
        {
            string result = string.Empty;

            //switch (j)
            //{
            //    case 1:
            //        result = "One";
            //        break;

            //    case 2:
            //        result = "Two";
            //        break;

            //    case 3:
            //        result = "Three";
            //        break;

            //    case 4:
            //        result = "Four";
            //        break;

            //    default:
            //        result = "Empty";
            //        break;
            //}

            string argument = "abc";

            switch (argument)
            {
                case "apple":
                case "pear":
                case "grape":
                    result = "fruit";
                    break;

                case "beet-root":
                case "potato":
                case "pumpkin":
                    result = "vegetable";
                    break;

                default:
                    result = "I don't know : " + argument;
                    break;
            }

            return result;
        }

        private static void Test()
        {
            List<string> listTemp = new List<string>() { "Apple", "Banana", "Banana" };
            HashSet<string> hsValues = new HashSet<string>();

            // Error free
            for (int i = listTemp.Count - 1; i >= 0; i--)
            {
                string item = listTemp[i];

                if (hsValues.Contains(item))
                {
                    listTemp.Remove(item);
                }
            }

            // Exception
            foreach (var item in listTemp)
            {
                if (hsValues.Contains(item))
                {
                    listTemp.Remove(item);
                }
            }

            //var i = 10;

            //i = i - 10;
            //i -= 10;
        }
    }
}

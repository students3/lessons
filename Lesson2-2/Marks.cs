﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessons
{
    public enum Marks: byte
    {
        Unsatisfactory = 3,
        Satisfactory,
        Good,
        Excellent
    }

    public enum SellQty
    {
        Single = 1,
        InBatch3 = 3,
        InBatch5 = 5
    }


}

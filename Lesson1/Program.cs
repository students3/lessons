﻿using Business;
using Business.Interfaces;
using Business.Model;
using System;

namespace Lesson1
{
    sealed class Program
    {
        static void Main(string[] args)
        {
            IProduct product = new Cake
            {
                ProductName = "Barnie",
                Manufacturer = "Barnie factory",
                Price = 6.75,
                DateCreated = new DateTime(2017, 8, 1)
            };

            IManager manager = new CakeManager();
            manager.SellProduct(product, 3);

            Console.WriteLine("Product " + product.ProductName + "\n" +
                              "sold on " + product.DateSold?.ToString("yyyy MM") + "\n" +
                              "in qty - " + product.Qty);

            Console.ReadLine();
        }

        private IProduct[] GenerateProducts()
        {
            IProduct cake = new Cake();
            IProduct laptop = new Laptop();
            
            return new IProduct[] { cake, laptop };
        }

        private void SellProducts(IProduct[] products)
        {
            //call manager of needed type of Product
            // cake => CakeManager
            // laptop => LaptopManager

            for (int i = 0; i < products.Length; i++)
            {
                var item = products[i];

                if (item is Cake)
                {

                }
                else if (item is Laptop)
                {

                }
            }
        }

        private void PrintProducts(IProduct[] products)
        {
            for (int i = 0; i < products.Length; i++)
            {
                var product = products[i];

                string message = "Product " + product.ProductName + "\n" +
                              "sold on " + product.DateSold?.ToString("yyyy MM") + "\n" +
                              "in qty - " + product.Qty;
                Console.WriteLine(message);
            }
        }

        int GetMonthIndex(DateTime input)
        {
            return input.Month;
        }
    }
}

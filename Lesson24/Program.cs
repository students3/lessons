﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Lesson24.Cache;

namespace Lesson24
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductsCache.Products = new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "Soccer ball",
                    Price = 500,
                    Qty = 10
                }
            };

            UpdatePriceConcurrent();
            //AddProductsConcurrent();

            Console.ReadLine();
        }

        private static void UpdatePriceConcurrent()
        {
            var thread2 = new System.Threading.Thread(DeleteProduct1);
            var thread1 = new System.Threading.Thread(UpdatePrice5);

            thread2.Start();
            thread1.Start();

            Console.WriteLine("Threads started");

            while (thread1.IsAlive || thread2.IsAlive)
            {
                System.Threading.Thread.Sleep(1000);
            }

            Console.WriteLine("\r\n");
            var product = ProductsCache.Products.FirstOrDefault(x => x.Id == 1);
            Console.WriteLine($"Finished. Product Price {product?.Price.ToString("N2")}");
        }

        private static void AddProductsConcurrent()
        {
            var thread1 = new System.Threading.Thread(AddNewProduct1);
            var thread2 = new System.Threading.Thread(AddNewProduct2);

            thread1.Start();
            thread2.Start();
            Console.WriteLine("Threads started");

            while (thread1.IsAlive || thread2.IsAlive)
            {
                System.Threading.Thread.Sleep(1000);
            }

            Console.WriteLine("\r\n");
            Console.WriteLine($"Finished. Product qty {ProductsCache.Products.Count}");
        }

        private static void UpdatePrice5()
        {
            ProductsCache.UpdatePrice(1, 5);
            Console.WriteLine("Updated Price to 5.00");
        }

        private static void UpdatePrice10()
        {
            ProductsCache.UpdatePrice(1, 10);
            Console.WriteLine("Updated Price to 10.00");
        }

        private static void AddNewProduct1()
        {
            var product = new Product
            {
                Id = 2,
                Name = "Laptop",
                Price = 500,
                Qty = 10
            };

            ProductsCache.AddProduct(product);
            Console.WriteLine("Added laptop");
        }

        private static void AddNewProduct2()
        {
            var product = new Product
            {
                Id = 3,
                Name = "Dress",
                Price = 1500,
                Qty = 10
            };

            ProductsCache.AddProduct(product);
            Console.WriteLine("Added dress");
        }

        private static void DeleteProduct1()
        {
            ProductsCache.DeleteProduct(1);
        }

        private void BlockingCollections()
        {
            var coll1 = new BlockingCollection<string>();
            var coll2 = new ConcurrentBag<string>();

            var list = coll1.GetConsumingEnumerable().ToList();

        }
    }
}

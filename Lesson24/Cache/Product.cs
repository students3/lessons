﻿namespace Lesson24.Cache
{
    public class Product
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public double Price { get; set; }
        public int Qty { get; set; }
    }
}

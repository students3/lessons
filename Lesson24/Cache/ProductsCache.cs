﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Lesson24.Cache
{
    public static class ProductsCache
    {
        public static List<Product> Products { get; set; }

        public static void UpdatePrice(int id, double price)
        {
            lock (Products)
            {
                var product = Products.FirstOrDefault(x => x.Id == id);
                if (product != null) product.Price = price;
            }
        }

        public static void UpdateQty(int id, int qty)
        {
            Monitor.Enter(Products);
            var product = Products.FirstOrDefault(x => x.Id == id);
            if (product != null) product.Qty = qty;
            Monitor.Exit(Products);
        }

        public static void AddProduct(Product product)
        {
            lock (Products)
            {
                Products.Add(product);
            }
        }

        public static void DeleteProduct(int id)
        {
            var product = Products.FirstOrDefault(x => x.Id == id);
            if (product != null) Products.Remove(product);
        }
    }
}

﻿using Lesson21.Attributes;
using Lesson21.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Lesson21.Models;


namespace Lesson21.Managers
{
    public class ReflectionManager
    {
        private readonly Assembly _asm;

        public ReflectionManager(string assemblyName)
        {
            _asm = Assembly.Load(assemblyName);
        }

        public ReflectionManager(Assembly asm)
        {
            _asm = asm;
        }


        public void GetAssemblyInfo()
        {
            Console.WriteLine("");
        }

        public void GetAssemblyTypes()
        {
            Type[] types = _asm.GetTypes();

            foreach (var itm in types)
            {
                Console.WriteLine("Type: {0}", itm);
                Console.WriteLine("Methods list:");

                this.ListMethods(itm);

                Console.WriteLine("================================================== \r\n");
            }
        }

        public void GetEnumStringAttributes()
        {
            var stringValues = typeof(StringValueAttribute);
            var t = typeof(ProjectStatus);
            var propInfo = stringValues.GetProperty("Value");

            foreach (var field in t.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                object[] objs = field.GetCustomAttributes(stringValues, false);
                var value = field.GetValue(null);
                var attrValues = objs.Select(x => propInfo.GetValue(x, null).ToString());

                //var attrValues = new List<string>();

                //foreach (object ob in objs)
                //{
                //    attrValues.Add(PropInfo.GetValue(ob, null).ToString());
                //}

                Console.WriteLine($"Enum value: {value}, attributes: {string.Join(" | ", attrValues)}");
            }
        }

        private void ListMethods(Type t)
        {
            MethodInfo[] meth = t.GetMethods();

            foreach (MethodInfo me in meth)
            {
                string retVal = me.ReturnType.FullName;
                string prm = "( ";
                foreach (ParameterInfo pi in me.GetParameters())
                {
                    prm += string.Format("type " + pi.ParameterType + " " + pi.Name);
                }

                prm += " )";

                Console.WriteLine("Full name " + retVal + "." + me.Name + prm);
            }
        }

        public void GetPersonsExpressionTree()
        {
            Person p1 = new Person { Name = "Kayes", Age = 29, JoiningDate = DateTime.Parse("2010-06-06") };
            Person p2 = new Person { Name = "Gibbs", Age = 34, JoiningDate = DateTime.Parse("2008-04-23") };
            Person p3 = new Person { Name = "Steyn", Age = 28, JoiningDate = DateTime.Parse("2011-02-17") };

            List<Person> persons = new List<Person>();
            persons.Add(p1);
            persons.Add(p2);
            persons.Add(p3);

            string sortByProp = "Age";
            Type sortByPropType = typeof(Person).GetProperty(sortByProp).PropertyType;

            ParameterExpression pe = Expression.Parameter(typeof(Person), "p");
            var expr = Expression.Lambda(Expression.Property(pe, sortByProp), pe);

            IQueryable<Person> query = persons.AsQueryable();

            MethodInfo orderByMethodInfo = typeof(Queryable).GetMethods(BindingFlags.Public | BindingFlags.Static)
                                                           .Single(mi => mi.Name == "OrderBy" && mi.IsGenericMethodDefinition
                                                                                              && mi.GetGenericArguments().Length == 2
                                                                                              && mi.GetParameters().Length == 2
            );

            List<Person> sortedList = (orderByMethodInfo.MakeGenericMethod(new Type[] { typeof(Person), sortByPropType })
                                                        .Invoke(query, new object[] { query, expr }) as IOrderedQueryable<Person>).ToList();

            List<Person> sortedList2 = persons.OrderBy(x => x.Age).ToList();
        }
    }
}

﻿using System;

namespace Lesson21
{
    public class PreprocessorDirectives
    {
#if DEBUG
        public void Calculate(int x, int y)
        {
            Console.WriteLine(x * y);
        }
#else
        public void Calculate(int x, int y)
        {
            Console.WriteLine(x * y * 2);
        }
#endif

        // #if, #else, #elif, #endif, #define, #under,
        // #warning, #error, #line, #region,
        // #endregion, #pragma, #pragma warning,
        // #pragma checksum
    }
}

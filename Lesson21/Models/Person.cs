﻿using System;

namespace Lesson21.Models
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime JoiningDate { get; set; }
    }
}

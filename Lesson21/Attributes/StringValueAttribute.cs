﻿using System;

namespace Lesson21.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public sealed class StringValueAttribute : Attribute
    {
        private readonly string _value;

        public string Value
        {
            get { return _value; }
        }

        public StringValueAttribute(string value)
        {
            _value = value;
        }
    }
}

﻿using Lesson21.Managers;
using System;
using t1 = System.Threading.Tasks;

namespace Lesson21
{
    class Program
    {
        static void Main(string[] args)
        {
            //Preprocessor();
            //TestReflectionMethods();
            //TestReflectionEnum();
            TestReflectionTree();


            Console.WriteLine("Press any key ...");
            Console.ReadLine();
        }

        private static void TestUsing()
        {
            //using (resource)
            //{

            //}

            t1.Task myTask = new t1.Task(() =>
            {
                Console.WriteLine("Hello world");
            });

            myTask.Wait();
        }

        private static void Preprocessor()
        {
            var myClass = new PreprocessorDirectives();
            myClass.Calculate(2, 3);
        }

        private static void TestReflectionAssembly()
        {
            var reflectionManager = new ReflectionManager("EF.CodeFirst");
            reflectionManager.GetAssemblyInfo();
        }

        private static void TestReflectionMethods()
        {
            var reflectionManager = new ReflectionManager("EF.CodeFirst");
            reflectionManager.GetAssemblyTypes();
        }

        private static void TestReflectionEnum()
        {
            var reflectionManager = new ReflectionManager(System.Reflection.Assembly.GetExecutingAssembly());
            reflectionManager.GetEnumStringAttributes();
        }

        private static void TestReflectionTree()
        {
            var reflectionManager = new ReflectionManager(System.Reflection.Assembly.GetExecutingAssembly());
            reflectionManager.GetPersonsExpressionTree();
        }
    }
}

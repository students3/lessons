﻿namespace Lesson4.Abstract
{
    public abstract class AbstractPlaneFactory
    {
        private string _planeFactoryName;

        public AbstractPlaneFactory(string planeFactory)
        {
            _planeFactoryName = planeFactory;
        }

        public abstract void ConstructPlaneBody();

        public abstract void ConstructWings();

        public abstract void ConstructChassis();

        public abstract void InstallEngines();

        public abstract void InstallChairs();

        public void ConstructPlane()
        {
            ConstructPlaneBody();
            ConstructWings();
            ConstructChassis();
            InstallChairs();
            InstallEngines();

            SayHello();
        }

        private void SayHello()
        {

        }
    }
}

﻿using System;

namespace Lesson4.Abstract
{
    public class SmallPlaneFactory : AbstractPlaneFactory
    {
        public SmallPlaneFactory(string planeFactory) : base(planeFactory)
        {
        }

        public override void ConstructChassis()
        {
            Console.WriteLine("Construct Chassis");
        }

        public override void ConstructPlaneBody()
        {
            Console.WriteLine("Construct PlaneBody");
        }

        public override void ConstructWings()
        {
            Console.WriteLine("Construct Wings");
        }

        public override void InstallChairs()
        {
            Console.WriteLine("Install Chairs");
        }

        public override void InstallEngines()
        {
            Console.WriteLine("Install small Engines");
        }
    }
}

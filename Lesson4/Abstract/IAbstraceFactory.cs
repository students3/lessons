﻿namespace Lesson4.Abstract
{
    public interface IAbstraceFactory
    {
        void ConstructPlaneBody();

        void ConstructWings();

        void ConstructChassis();

        void InstallEngines();

        void InstallChairs();
    }
}

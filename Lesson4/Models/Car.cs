﻿using System;
using System.Data;

namespace Lesson4.Models
{
    public class Car: IDisposable
    {
        //public Car()
        //{
        //}

        private DataTable dtTemp;
        //private Bitmap

        [Obsolete]
        public Car(string make)
        {
            this.Make = make;
        }

        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }

        [Obsolete("Use another method")]
        public void DoSomething()
        {

        }

        ~Car()
        {
            dtTemp?.Dispose();
            Console.WriteLine("Disposing car ...");
        }

        public void Dispose()
        {
            //if (dtTemp != null) dtTemp.Dispose();
            dtTemp?.Dispose();
            Console.WriteLine("Disposing car ...");
        }
    }
}

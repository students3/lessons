﻿using System;
using System.Collections.Generic;

namespace Lesson4.Models
{
    public class House
    {
        private List<HouseLevel> _levels = new List<HouseLevel>();
        
        public void AddLevel(HouseLevel level)
        {
            _levels.Add(level);
        }

        public void DeleteLevel(HouseLevel level)
        {
            _levels.Remove(level);
        }

        public double GetHousePrice()
        {
            double result = 0;

            foreach (var item in _levels)
            {
                result += item.Cost;
            }

            return result;
        }

        public int Levels
        {
            get
            {
                return _levels.Count;
            }
            set
            {
                throw new NotSupportedException("Canot do this");
            }
        }
    }
}

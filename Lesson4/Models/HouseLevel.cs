﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson4.Models
{
    public class HouseLevel
    {
        public string Name { get; set; }
        public int Cost { get; set; }
    }
}

﻿namespace Lesson4
{
    public struct Animal
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}

﻿using System.Collections.Generic;
using Lesson4.Models;
using System.Linq;

namespace Lesson4.Bot
{
    public class YearThenMakeCarBot : AbstractBot
    {
        public override void SendReport(Car[] arrCars)
        {
            Dictionary<int, Car[]> dicTemp = this.GroupCarsByYear(arrCars);

            foreach (var item in dicTemp)
            {
                System.Console.WriteLine("Year: " + item.Key);
                Dictionary<string, Car[]> dicCars = this.GroupCarsByMake(item.Value);

                foreach (var item2 in dicCars)
                {
                    System.Console.WriteLine("Make: " + item2.Key);
                    System.Console.WriteLine("Models: " + string.Join(", ", item2.Value.Select(x => x.Model).ToArray()));
                }
            }

            // TODO: 
        }
    }
}

﻿using Lesson4.Models;
using System.Collections.Generic;
using System.Linq;

namespace Lesson4.Bot
{
    public abstract class AbstractBot
    {
        public abstract void SendReport(Car[] arrCars);

        public Dictionary<int, Car[]> GroupCarsByYear(Car[] arrCars)
        {
            Dictionary<int, Car[]> result = new Dictionary<int, Car[]>();

            result = arrCars.GroupBy(x => x.Year)
                            .ToDictionary(x => x.Key, x => x.ToArray());

            return result;
        }

        public Dictionary<string, Car[]> GroupCarsByMake(Car[] arrCars)
        {
            Dictionary<string, Car[]> result = new Dictionary<string, Car[]>();

            result = arrCars.GroupBy(x => x.Make)
                            .ToDictionary(x => x.Key, x => x.ToArray());

            return result;
        }
    }
}

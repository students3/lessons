﻿using Lesson4.Abstract;
using Lesson4.Models;
using System;

namespace Lesson4
{
    class Program
    {
        static void Main(string[] args)
        {
            TestPlaneFactory();
            
            Console.ReadLine();
        }

        private static void TestStructures()
        {
            Animal animal1 = new Animal();
            animal1.Age = 21;
            animal1.Name = "Bobik";

            Animal animal2 = animal1;
            animal2.Age = 30;
            animal2.Name = "Sharik";

            Car car1 = new Car("BMW");
            //car1.Make = "BMW";
            car1.Model = "328i";
            car1.DoSomething();

            Car car2 = new Car("Mercedes"); ;
            //car2.Make = "Mercedes";
            car2.Model = "CLK-Class";

            Console.WriteLine("Animal1 age: " + animal1.Age);
            Console.WriteLine("Animal1 name: " + animal1.Name);
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("Animal2 age: " + animal2.Age);
            Console.WriteLine("Animal2 name: " + animal2.Name);


            Console.WriteLine("\n\n");
            Console.WriteLine("Car1 Make: " + car1.Make);
            Console.WriteLine("Car1 Model: " + car1.Model);
            Console.WriteLine(new string('-', 50));
            Console.WriteLine("Car2 Make: " + car2.Make);
            Console.WriteLine("Car2 Model: " + car2.Model);

            //car1.Dispose();
            //car2.Dispose();

        }

        private static void TestGetters()
        {
            House house = new House();
            house.AddLevel(new HouseLevel
            {
                Name = "First level",
                Cost = 50000
            });

            int levels = house.Levels;
            house.Levels = 5;  // error
        }

        private static void TestPlaneFactory()
        {
            SmallPlaneFactory factory = new SmallPlaneFactory("smallFactory");
            factory.ConstructPlane();
        }
    }
}
